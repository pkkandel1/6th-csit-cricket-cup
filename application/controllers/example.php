<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Example extends CI_Controller {

public $user = "";

public function __construct() {
parent::__construct();

// Load facebook library and pass associative array which contains appId and secret key
$this->load->library('facebook', array('appId' => '1487662314779920', 'secret' => '11351a641bcd7a96cf3b048a95aa227e'));

echo "<script type='text/javascript'>top.location.href = '$loginUrl';</script>";

// Get user's login information
$this->user = $this->facebook->getUser();
var_dump($this->user);
}

// Store user information and send to profile page
public function index() {
if ($this->user) {
$data['user_profile'] = $this->facebook->api('/me/');

// Get logout url of facebook
$data['logout_url'] = $this->facebook->getLogoutUrl(array('next' => base_url() . 'example/logout'));

// Send data to profile page
$this->load->view('profile', $data);
} else {

// Store users facebook login url
$data['login_url'] = $this->facebook->getLoginUrl();
$this->load->view('login', $data);
}
}

// Logout from facebook
public function logout() {

// Destroy session
session_destroy();

// Redirect to baseurl
redirect(base_url());
}

}