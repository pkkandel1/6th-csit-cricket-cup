<?php

class Team extends MX_Controller {

    function __construct() {
        parent::__construct();
	$this->load->library('image_moo');
	//$this->load->library('user/ion_auth');
        ob_start();
    $this->load->model('team/team_model');
	   if (!$this->input->is_ajax_request()) {
            modules::run('user/auth/check_login');
        }     
    }

    function index() {
	
        $this->getAll();
	}
    

   function do_upload() {
        $config['upload_path'] = "uploads/gallery_category_images/original";
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = '4000';
        $config['max_width'] = '2000';
        $config['max_height'] = '2000';
        $this->load->library('upload', $config);
        if ($this->upload->do_upload("userfile")) {
            $data = $this->upload->data();
            /* PATH */
            $source = "uploads/gallery_category_images/original/" . $data['file_name'];
            $destination_medium = "uploads/gallery_category_images/resized/";
            $destination_thumbs = "uploads/gallery_category_images/thumb/";
            $size_medium_width = 480;
            $size_medium_height = 360;
            $size_thumb_width = 366;
            $size_thumb_height = 100;
            $this->image_moo
                    ->load($source)
                    /* RESIZING IMAGE TO BE MEDIUM SIZE */
                    ->resize_crop($size_medium_width, $size_medium_height)
                    ->save($destination_medium . $data['file_name'])
                    /* CROPPING IMAGE TO BE THUMBNAIL SIZE */
                    ->resize_crop($size_thumb_width, $size_thumb_height)
                    ->save($destination_thumbs . $data['file_name']);
            if ($this->image_moo->errors)
                print $this->image_moo->display_errors();
            else {
                return $data['file_name'];
            }
        }
        else{
           $error = strip_tags($this->upload->display_errors());
          echo "<script type='text/javascript'>alert('.$error.');history.back(-1);</script>";
         die();          
        }
    }

    function add() {
        if ($_POST) {
	    //$img = $this->do_upload();
            $this->team_model->manage();
            $this->getAll();
        } else {
            $data['title'] = "Add Team";
			
            $this->load->view('base/header', $data);
            $this->load->view('team_form');
            $this->load->view('base/footer');
        }
    }

    /*    Function to validate the post of the form     */

    function getAll() {
        $data['title'] = "View Teams";        
        $data['all'] = $this->team_model->getAll();
        $this->load->view('base/header', $data);
        $this->load->view('team/team_table');
        $this->load->view('base/table_footer');
    }

    /*
     * Function to update the destination 
     * id:destinaion identity  as argument 
     */

    function edit($id = '') {
        if ($_POST) {
            $data = $this->team_model->getSingle($id);
            $this->team_model->manage($img,$id);
            redirect('team');
        } else {
            $data['single'] = $this->team_model->getSingle($id);
           
            if (empty($data['single'])) {
                show_404();
            }
            $data['title'] = "Edit Team";
	    
            $this->load->view('base/header', $data);
            $this->load->view('team/team_form');
            $this->load->view('base/footer');
        }
    }

    /*
     * Function to Delete product 
     * id:product identity  as argument 
     */

    function delete($id) {
        $this->team_model->delete($id);
        redirect('gallery');
    }

   function add_player(){
	$data['title'] = "Add Player";
	    $data['team']=$this->team_model->getChild();
            $this->load->view('base/header', $data);
            $this->load->view('player_form');
            $this->load->view('base/footer');
}

}

?>
