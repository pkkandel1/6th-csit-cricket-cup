<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo base_url();?>dashboard"><i class="entypo-home"></i>Home</a>
    </li>
    <li>

        <a href="<?php echo base_url();?>team/player">Image</a>
    </li>
    <li class="active">

        <strong>Add</strong>
    </li>
</ol>
<a href="<?php echo base_url(); ?>team/player" class="btn btn-blue">
    <i class="entypo-search"></i>
    View Players
</a>
<h2>Add Player</h2>
<br />

<div class="panel panel-primary">

    <div class="panel-heading">
        <div class="panel-title">All fields have validation rules <small></small></div>

        <div class="panel-options">
            <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i class="entypo-cog"></i></a>
            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
            <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
            <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
        </div>
    </div>

    <div class="panel-body">

        <form role="form" id="form1" method="post" class="validate" action="" enctype="multipart/form-data">

           <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">Player Name</label>

                <input type="text" class="form-control" name="player_name" data-validate="required" data-message-required="This field is required" placeholder="Player name here" value="<?php if(!empty($single)){ echo $single->player_name;}?>"/>
            </div>
        </div> 

        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">Images</label><br>

                <input type="file" name="player_image" class="form-control file2 inline btn btn-primary" multiple=""><font color="red" data-label="<i class='glyphicon glyphicon-circle-arrow-up'></i> &nbsp;Browse Files"></font>

            </div>
        </div> 

        <div class="col-md-6">
           <div class="form-group">
            <label class="control-label">Select Team: </label>

            <select class="form-control" name="team_id"> 
             <?php foreach($team as $t): ?>
               <option value="<?php echo $t->id;?>" <?php if(!empty($single)){ if($single->team_id == $t->id){echo 'selected="selected"';}}?>> <?php echo $t->team_name;?> </option>
           <?php endforeach;?>
       </select>
   </div>
</div>	    


<div class="col-md-11">
   <div class="form-group">
    <label class="control-label">Description</label>
    <textarea class="form-control ckeditor" name="description" data-validate="required">
        <?php if (!empty($single->description)) {
            echo $single->description;
        } else {
            echo set_value('description');
        }
        ?>
    </textarea>
</div>
</div>


<div class="col-md-6">
    <div class="form-group">
        <label class="control-label">Age</label>

        <input type="number" class="form-control" name="age" data-validate="required" data-message-required="This field is required" placeholder="Age" value="<?php if(!empty($single)){ echo $single->age;}?>"/>
    </div>
</div>


<div class="col-md-6">
    <div class="form-group">
        <label class="control-label">Handedness</label>
        <select class="form-control" name="handedness"> 
            <option name="handedness" value="right">Right handed </option>
            <option name="handedness" value="left">Left handed </option>
        </select>
    </div>
</div>


<div class="col-md-6">
    <div class="form-group">
    <label class="control-label">Playing Role</label>
        <select class="form-control" name="playing_role"> 
            <option name="role" value="batsman">Batsman </option>
            <option name="role" value="bowler">Bowler </option>
            <option name="role" value="allrounder">All Rounder </option>
        </select>
    </div>
</div>

<div class="col-md-6">
    <div class="form-group">
        <label class="control-label">Batting Style</label>
        <select class="form-control" name="batting_style"> 
            <option name="bat" value="right">Right handed </option>
            <option name="bat" value="left">Left handed </option>
        </select>
    </div>
</div>


<div class="col-md-6">
    <div class="form-group">
        <label class="control-label">Balling Style</label>
        <select class="form-control" name="balling_style"> 
            <option name="bowl" value="right_pace">Right Arm Pace</option>
            <option name="bowl" value="right_off">Right Arm Off Spin</option>
            <option name="bowl" value="right_leg">Right Arm Leg Spin</option>
            <option name="bowl" value="right_slow">Right Arm Slow</option>
            <option name="bowl" value="left_pace">Left Arm Pace</option>
            <option name="bowl" value="left_off">Left Arm Off Spin</option>
            <option name="bowl" value="left_leg">Left Arm Leg Spin</option>
            <option name="bowl" value="left_slow">Left Arm Slow</option>

        </select>
    </div>
</div>

<div class="col-md-2">
        <div class="form-group">
            <label class="control-label">Publish</label>
            <ul class="icheck-list">
                <li>
                    <input tabindex="5" type="checkbox" class="icheck" name="publish" value="1" id="minimal-checkbox-1" <?php if(!empty($single)){ if($single->publish=="1"){echo "checked";}}?>>
                    <label for="minimal-checkbox-1"></label>
                </li>

            </ul>
        </div>
    </div>





<div class="col-md-11">
  <div class="form-group">
    <button type="submit" class="btn btn-success" name="submit">Submit</button>
    <button type="reset" class="btn">Reset</button>
</div>
</form>

</div>
</div>

</div><!-- Footer -->
