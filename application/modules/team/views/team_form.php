<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo base_url();?>dashboard"><i class="entypo-home"></i>Home</a>
    </li>
    <li>

        <a href="<?php echo base_url(); ?>team">Team</a>
    </li>
    <li class="active">

        <strong>Add</strong>
    </li>
</ol>
<a href="<?php echo base_url(); ?>team/" class="btn btn-blue">
    <i class="entypo-search"></i>
    View Team
</a>
<h2>Add Team</h2>
<br />

<div class="panel panel-primary">

    <div class="panel-heading">
        <div class="panel-title">All fields have validation rules <small></small></div>

        <div class="panel-options">
            <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i class="entypo-cog"></i></a>
            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
            <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
            <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
        </div>
    </div>

    <div class="panel-body">

        <form role="form" id="form1" method="post" class="validate" action="" enctype="multipart/form-data">

           <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">Team Name</label>

                <input type="text" class="form-control" name="team_name" data-validate="required" data-message-required="This field is required" placeholder="Team Name" value="<?php if(!empty($single)){ echo $single->team_name;}?>"/>
            </div>
        </div> 

        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">College Name</label>

                <input type="text" class="form-control" name="college_name" data-validate="required" data-message-required="This field is required" placeholder="College Name" value="<?php if(!empty($single)){ echo $single->college_name;}?>"/>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
            <label class="control-label">League Group</label>
                <select class="form-control" name="league_group"> 
                    <option name="group" value="1">A</option>
                    <option name="group" value="2">B</option>
                    <option name="group" value="3">C</option>
                    <option name="group" value="4">D</option>
                </select>
            </div>
        </div> 





        <div class="col-md-11">
           <div class="form-group">
            <label class="control-label">Description</label>
            <textarea class="form-control ckeditor" name="description" data-validate="required">
                <?php if (!empty($single->description)) {
                    echo $single->description;
                } else {
                    echo set_value('description');
                }
                ?>
            </textarea>
        </div>
    </div>


    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">Manager</label>

            <input type="text" class="form-control" name="manager" data-validate="required" data-message-required="This field is required" placeholder="Manager" value="<?php if(!empty($single)){ echo $single->manager;}?>"/>
        </div>
    </div>


    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">Contact No.</label>

            <input type="number" class="form-control" name="contact" data-validate="required" data-message-required="This field is required" placeholder="Contact" value="<?php if(!empty($single)){ echo $single->contact;}?>"/>
        </div>
    </div>


    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">Coach</label>

            <input type="text" class="form-control" name="coach" data-validate="required" data-message-required="This field is required" placeholder="Coach" value="<?php if(!empty($single)){ echo $single->coach;}?>"/>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">Physio</label>

            <input type="text" class="form-control" name="physio" data-validate="required" data-message-required="This field is required" placeholder="Physio" value="<?php if(!empty($single)){ echo $single->physio;}?>"/>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">Captain</label>

            <input type="text" class="form-control" name="captain" data-validate="required" data-message-required="This field is required" placeholder="Captain" value="<?php if(!empty($single)){ echo $single->captain;}?>"/>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">Vice Captain</label>

            <input type="text" class="form-control" name="vice_captain" data-validate="required" data-message-required="This field is required" placeholder="Vice Captain" value="<?php if(!empty($single)){ echo $single->vice_captain;}?>"/>
        </div>
    </div>


    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">Wicket Keeper</label>

            <input type="text" class="form-control" name="wicket_keeper" data-validate="required" data-message-required="This field is required" placeholder="Wicket Keeper" value="<?php if(!empty($single)){ echo $single->wicket_keeper;}?>"/>
        </div>
    </div>


    <div class="col-md-2">
        <div class="form-group">
            <label class="control-label">Publish</label>
            <ul class="icheck-list">
                <li>
                    <input tabindex="5" type="checkbox" class="icheck" name="publish" value="1" id="minimal-checkbox-1" <?php if(!empty($single)){ if($single->publish=="1"){echo "checked";}}?>>
                    <label for="minimal-checkbox-1"></label>
                </li>

            </ul>
        </div>
    </div>

    

    <div class="col-md-11">
      <div class="form-group">
        <button type="submit" class="btn btn-success" name="submit">Submit</button>
        <button type="reset" class="btn">Reset</button>
    </div>
</form>

</div>
</div>

</div><!-- Footer -->
