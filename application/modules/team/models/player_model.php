<?php

class Player_model extends CI_Model {

    private $table = "tbl_player_detail";

    function construct() {
        parent::__construct();
    }

    function index() {

        $this->load->view('admin/post');
    }

    /*
     * Function to add destination from form
     */

    function manage($img = '',$id = '') {


        $team_id = $this->input->post('team_id');
        $title = $this->input->post('name');

        $data = array(
            'team_id' => $this->input->post('team_id'),
            'player_name' => $this->input->post('player_name'),
            'player_image' =>$img,
            'description' => $this->input->post('description'),
            'age' => $this->input->post('age'),
            'handedness' => $this->input->post('handedness'),
            'playing_role' => $this->input->post('playing_role'),
            'batting_style' => $this->input->post('batting_style'),
            'balling_style' => $this->input->post('balling_style'),
            'publish' => $this->input->post('publish'),

            );

        if ($id == '') {

            $this->db->insert($this->table, $data);
            $this->session->set_flashdata('message', "Insertion successfull");
        } else {
            $this->db->where('id', $id);
            $this->db->update($this->table, $data);
            $this->session->set_flashdata('message', "Information updated successfully");
        }
    }

    function getAll() {
        $query = $this->db->query("SELECT p.*,t.team_name as tname from tbl_player_detail p INNER JOIN tbl_team_detail t on t.id = p.team_id ORDER BY p.id Desc limit 15");
        return $query->result();
    }

    function getSingle($id) {
        $data = $this->db->query("SELECT * FROM $this->table WHERE id='$id'");
        return $data->row($id);
    }

    function getAccToTeam($id) {
        //$id = $this->team_model->slugtoid($slug);       
        $data = $this->db->query("SELECT * from $this->table WHERE team_id='$id' ORDER BY id DESC");
        return $data->result();
    }

    function delete($id) {
        $this->db->delete($this->table, array('id' => $id));
        $this->session->set_flashdata('message', "Information Deleted successfully");
    }

    function search($search) {
        $data = $this->db->query("select * from $this->table where name like '$search%'");
        return $data->result();
    }

   

}

?>
