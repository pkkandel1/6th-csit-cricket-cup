<?php

class Team_model extends CI_Model {

    private $table = "tbl_team_detail";

    function construct() {
        parent::__construct();
    }

    function index() {

        $this->load->view('admin/post');
    }

    function manage($id = '') {
        $name = $this->input->post('name');
        $title = $this->input->post('name');
        $slug = url_title($title, 'dash', true);

        $data = array(
            'team_name' => $this->input->post('team_name'),
            'college_name' => $this->input->post('college_name'),
            'description' => $this->input->post('description'),
            'league_group' => $this->input->post('league_group'),
            'manager' => $this->input->post('manager'),
            'contact' => $this->input->post('contact'),
            'coach' => $this->input->post('coach'),
            'physio' => $this->input->post('physio'),
            'captain' => $this->input->post('captain'),
            'vice_captain' => $this->input->post('vice_captain'),
            'wicket_keeper' => $this->input->post('wicket_keeper'),
            'publish' => $this->input->post('publish'),
        );

        if ($id == '') {

            $this->db->insert($this->table, $data);
            $this->session->set_flashdata('message', "Insertion successfull");
        } else {
            $this->db->where('id', $id);
            $this->db->update($this->table, $data);
            $this->session->set_flashdata('message', "Information updated successfully");
        }
    }

    function getAll() {
        $this->db->select('*');
        $this->db->from($this->table);
        $query = $this->db->get();
        return $query->result();
    }

    function getSingle($id) {
        $data = $this->db->query("SELECT * FROM $this->table WHERE id='$id'");
        return $data->row($id);
    }

    function slugtoid($slug) {

        $data = $this->db->query("SELECT id FROM $this->table WHERE slug='$slug'");

        //print_r($data); die();
        $cat_id = $data->row($slug);
        if (!empty($cat_id)) {
            return $cat_id->id;
        }
    }

    function delete($id) {
        $this->db->delete($this->table, array('id' => $id));
        $this->session->set_flashdata('message', "Information Deleted successfully");
    }

    function search($search) {
        $data = $this->db->query("select * from $this->table where name like '$search%'");
        return $data->result();
    }

}

?>
