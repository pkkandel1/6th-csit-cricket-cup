<?php

class Home extends MX_Controller {

    function __construct() {
        parent::__construct();
        
    }

    function index() {
        $this->load->view('home/index');
    }

  function page($slug = '') {
        
        if (!empty($slug)) {
            $data['images'] = $this->image_model->getAll();
            $data['news'] = $this->page_model->getAllNewsEvents();
            $data['page'] = $this->page_model->getSinglePageBySlug($slug);
            if(!empty($data['page']->meta_keyword)){
            $data['meta_keywords'] = $data['page']->meta_keyword;
            }
            if(!empty($data['page']->meta_title)){
             $data['meta_title'] = $data['page']->meta_title;
             }
             if(!empty($data['page']->meta_description)){
            $data['meta_description'] = $data['page']->meta_description;
            }
            if(empty ($data['page'] )){
            $this->show_404();
            }
            else{

            $this->load->view('page', $data);
            }
        } else {
            $this->show_404();
        }
    }

    function news() {
        $data['title'] = 'News and Events of Kamyak school';
        $data['images'] = $this->image_model->getAll();
        $data['news'] = $this->page_model->getAllNewsEvents();
        $this->load->view('home/news', $data);
    }
     
    

    function show_404() {


        $this->load->view('home/404');
       
    }

    function send_contact() {
        $this->home_model->send_contact();
    }

    
    function sendMail(){
      $result =  $this->home_model->sendMail();
     // print_r($result);die();
      return $result;
        
    }

    public function search()
    {
        $search = $this->input->get('search');

        $data['result'] =$this->home_model->getAllSearch($search);
        $data['title'] = $search;
        $this->load->view('home/search',$data);
      
    }
}

?>