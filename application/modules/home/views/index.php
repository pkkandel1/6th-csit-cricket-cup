<?php include('includes/header.php'); ?>

  <!-- The top section -->
    <section class="top-sec">
     <span class="texas">(<a href="http://www.texasintl.edu.np" target="_blank">texas</a>)</span>
      <h2><span>6<sup>th</sup></span> csit inter college t20<br>cricket<br>tournament</h2> 
      <img src="<?php echo base_url();?>assets/home/images/cuppic.png" alt="Winning" class="win">
      <div class="bottom-part">
        <div class="bowling-side">
          <img src="<?php echo base_url();?>assets/home/images/bowler.png">
          <div class="wicket"></div>
        </div>

        <div class="batting-side">
          <img src="<?php echo base_url();?>assets/home/images/batsman.png">
          <div class="wicket"></div>
          <img src="<?php echo base_url();?>assets/home/images/wkeeper.png">
        </div>
      </div>
    </section>

    <!-- The slider container -->

    <section class="slider-container">
      <section id="slider"><!--slider-->
        <div class="container">
          <div class="row">
            <div class="col-sm-12">
              <div id="slider-carousel" class="carousel slide" data-ride="carousel" data-interval="20000">
                <ol class="carousel-indicators">
                  <li data-target="#slider-carousel" data-slide-to="0" class="active"></li>
                  <li data-target="#slider-carousel" data-slide-to="1"></li>
                  <li data-target="#slider-carousel" data-slide-to="2"></li>
                </ol>
                
                <div class="carousel-inner">
                  <div class="item active">
                    <div class="col-sm-12">
                      <h2 class="res-tit">Live score</h2>
                      <div class="small-res">
                        <span class="top-text">01st match, group stage group 1, Baisakh 5, 2073</span><br>
                        <h2><span>ASCOL</span> N/A</h2>
                        <h2><span>Prime</span> N/A <span class="br-text">(N/A ov, target N/A)</span></h2>
                        <div class="result-btm">XYZ requires N/A runs (with ABC balls remaining)</div>
                      </div>
                    </div>
                  </div> 

                  <div class="item">
                    <div class="col-sm-12">
                    <h2>upcoming</h2>
                      <div class="contents-low">bernhardt VS new summit</div>
                      <div class="contents-low">patan VS himalaya</div>
                    </div>
                  </div>
                  
                  <div class="item">
                    <div class="col-sm-12">
                      <h2 class="res-tit">Recent match</h2>
                      <div class="small-res">
                        <span class="top-text">1st match, group stage group 1, Baisakh 5, 2073</span><br>
                        <h2><span>Ascol</span> n/a</h2>
                        <h2><span>Prime</span> n/a <span class="br-text">(20.0 ov, target)</span></h2>
                        <div class="result-btm">ABC won by n/a wickets (with abc balls remaining)</div>
                      </div>

                    </div>
                  </div>
                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </section><!--/slider-->
    </section>

    <!-- Paragraph -->
    <section class="paras">
      <p>
        Like previous years, this year as well, the 6th CSIT T20 Cricket Tournament is here. This tournament is sacred to all <br>
        the CSIT students which is taken as a chance to know the co-eds of different CSIT colleges around the valley and as well<br> to improve the relation between the same graders.
      </p>
    </section>

    <!-- Hexagonal part with containers and everything -->
    <section class="hexagons">
      <div class="container">
        <div class="row">
          <div class="col-md-8 col-sm-12 col-xs-12 left-hexagons" id="tabs">
            <!-- Nav tabs -->
              <ul class="nav nav-tabs responsive" role="tablist">
                <li role="presentation" class="active"><a href="#updates" aria-controls="updates" role="tab" data-toggle="tab">
                  <div id="triangle-top1"></div>
                  <div class="object1">
                    <i class="livicon" data-name="notebook" data-size="50" data-color="#fff" data-hovercolor="false" data-duration="1000"
                    data-onparent="true" activeparentclass="active"></i>
                  </div>
                  <div id="triangle-bottom1"></div>
                  <span>Updates</span>
                </a></li>
                <li role="presentation"><a href="#teams" aria-controls="teams" role="tab" data-toggle="tab">
                  <div id="triangle-top2"></div>
                  <div class="object2">
                    <i class="livicon" data-name="users" data-size="50" data-color="#fff" data-hovercolor="false" data-duration="1000"></i>
                  </div>
                  <div id="triangle-bottom2"></div>
                  <span>Teams</span></a></li>
                <li role="presentation"><a href="#fixtures" aria-controls="fixtures" role="tab" data-toggle="tab">
                  <div id="triangle-top3"></div>
                  <div class="object3">
                    <i class="livicon" data-name="alarm" data-size="50" data-color="#fff" data-hovercolor="false" data-duration="1000"></i>
                  </div>
                  <div id="triangle-bottom3"></div>
                  <span>Fixtures</span></a></li>
                <li role="presentation"><a href="#results" aria-controls="results" role="tab" data-toggle="tab">
                  <div id="triangle-top4"></div>
                  <div class="object4">
                    <i class="livicon" data-name="table" data-size="50" data-color="#fff" data-hovercolor="false" data-duration="1000"></i>
                  </div>
                  <div id="triangle-bottom4"></div>
                  <span>Results</span></a></li>
              </ul>

              <!-- Tab panes -->
              <div class="tab-content responsive">
                <div role="tabpanel" class="tab-pane active" id="updates">
                 <div class="back-grey">
                   <div class="news-short">
                     <h3>6th CSIT T20 Cricket Tournamnet to be organized by ASCOL</h3>
                     <span><i class="fa fa-clock-o"></i>5th Baisakh 2073</span>
                     <p class="desc">
                       The prestigeous CSIT T20 Cricket Tournament is being organized by Amrit Science Campus, the champion of 5th version, and is fixed to start from 5th of Baisakh of the year 2073.
                        <!-- <a href="score.html" target="_blank">Score Board <i class="fa fa-angle-double-right"></i></a> -->
                     </p>
                   </div>
                   <div class="news-short">
                     <h3>Texas to sponsor for the title this year</h3>
                     <span><i class="fa fa-clock-o"></i>5th Baisakh 2073</span>
                     <p class="desc">
                       The 6th CSIT T20 Cricket Tournament is being held with the umbrella name of Texas College. Texas College sponsored for the title of the upcoming tournament.
                       <!-- <a href="score.html" target="_blank">Score Board <i class="fa fa-angle-double-right"></i></a> -->
                     </p>
                   </div>
                   <div class="news-short">
                     <h3>12 teams to play for the trophy</h3>
                     <span><i class="fa fa-clock-o"></i>5th Baisakh 2073</span>
                     <p class="desc">
                     This year, 12 teams registered for the T20 Cricket Tournament. Including the previous versions finalist Bernhardt College, we have Prime, Sambriddhi, New Summit, Orchid, Patan, Himalayas, Texas, Henryford, CAB and Sagarmatha.
                       <!-- <a href="score.html" target="_blank">Score Board <i class="fa fa-angle-double-right"></i></a> -->
                     </p>
                   </div>
                   <div class="news-short">
                     <h3>All set for Tournament</h3>
                     <span><i class="fa fa-clock-o"></i>5th Baisakh 2073</span>
                     <p class="desc">
                     Everything is set and ready to set off for the Texas 6th CSIT Cricket Tournament. The teams have been picked and the fixtures are published. Get ready for the thrilling event to be start on first sunday of 2073. Come join us for more entertainment.
                       <!-- <a href="score.html" target="_blank">Score Board <i class="fa fa-angle-double-right"></i></a> -->
                     </p>
                   </div>
                 </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="teams">
                 <div class="back-grey">
                 <div class="top-two">
                   <div>
                     <h2>group 1</h2>
                     <ol>
                       <li>Ascol</li>
                       <li>Prime</li>
                       <li>Sambridhhi</li>
                     </ol>
                   </div>
                   <div></div>
                   <div>
                     <h2>group 2</h2>
                     <ol>
                       <li>Bernhardt</li>
                       <li>new summit</li>
                       <li>orchid</li>
                     </ol>
                   </div>
                  </div>
                  <div class="border-middle">
                  <div></div>
                  <div></div>
                  </div>
                  <div class="bottom-two">
                   <div>
                     <h2>group 3</h2>
                     <ol>
                       <li>patan</li>
                       <li>himalaya</li>
                       <li>texas</li>
                     </ol>
                   </div>
                   <div></div>
                   <div>
                     <h2>group 4</h2>
                     <ol>
                       <li>henryford</li>
                       <li>CAB</li>
                       <li>sagarmatha</li>
                     </ol> 
                   </div>
                  </div> 

                 </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="fixtures">
                <div class="back-grey">
                  <div class="total-flexbox">
                  <div class="one-fix">
                    <h3>Amrit Science Campus <span>v/s</span> Prime College</h3>
                    <p>April 17, 2016 | 05 Baishakh, 2073 (09:00 NPT)</p>
                    <i>- tournament match 1</i>
                    
                  </div>
                  <div class="one-fix">
                    <h3>Bernhardt <span>v/s</span> New Summit</h3>
                    <p>April 17, 2016 | 05 Baishakh, 2073 (13:00 NPT)</p>
                    <i>- tournament match 2</i>
                  </div>
                  <div class="one-fix">
                    <h3>Patan <span>v/s</span> Himalaya</h3>
                    <p>April 18, 2016 | 06 Baishakh, 2073 (09:00 NPT)</p>
                    <i>- tournament match 3</i>
                  </div>
                  <div class="one-fix">
                    <h3>Henryford <span>v/s</span> CAB</h3>
                    <p>April 18, 2016 | 06 Baishakh, 2073 (13:00 NPT)</p>
                    <i>- tournament match 4</i>
                  </div>
                  <div class="one-fix">
                    <h3>Bernhardt <span>v/s</span> Orchid</h3>
                    <p>April 19, 2016 | 07 Baishakh, 2073 (09:00 NPT)</p>
                    <i>- tournament match 5</i>
                  </div>
                  <div class="one-fix">
                    <h3>Prime <span>v/s</span> Sambridhhi</h3>
                    <p>April 19, 2016| 07 Baishakh, 2073 (13:00 NPT)</p>
                    <i>- tournament match 6</i>
                  </div>
                   <div class="one-fix">
                    <h3>Henryford <span>v/s</span> Sagarmatha</h3>
                    <p>April 20, 2016 | 08 Baishakh, 2073 (09:00 NPT)</p>
                    <i>- tournament match 7</i>
                  </div>
                   <div class="one-fix">
                    <h3>Himalaya <span>v/s</span> Texas</h3>
                    <p>April 20, 2016 | 08 Baishakh, 2073 (13:00 NPT)</p>
                    <i>- tournament match 8</i>
                  </div>
                   <div class="one-fix">
                    <h3>Amrit Science Campus <span>v/s</span> Sambridhhi</h3>
                    <p>April 21, 2016 | 09 Baishakh, 2073 (09:00 NPT)</p>
                    <i>- tournament match 9</i>
                  </div>
                   <div class="one-fix">
                    <h3>New Summit <span>v/s</span> Orchid</h3>
                    <p>April 21, 2016 | 09 Baishakh, 2073 (13:00 NPT)</p>
                    <i>- tournament match 10</i>
                  </div>

                   <div class="one-fix">
                    <h3>CAB <span>v/s</span> Sagarmatha</h3>
                    <p>April 22, 2016 | 10 Baishakh, 2073 (09:00 NPT)</p>
                    <i>- tournament match 11</i>
                  </div>

                   <div class="one-fix">
                    <h3>Patan <span>v/s</span> Texas</h3>
                    <p>April 22, 2016 | 10 Baishakh, 2073 (13:00 NPT)</p>
                    <i>- tournament match 12</i>
                  </div>

                   <div class="one-fix">
                    <h3>Winner of 'A' <span>v/s</span> Runner up of 'C'</h3>
                    <p>April 24, 2016 | 12 Baishakh, 2073 (09:00 NPT | 1st quarter final)</p>
                    <i>- tournament match 13</i>
                  </div>
                  
                  <div class="one-fix">
                    <h3>Winner of 'B' <span>v/s</span> Runner up of 'D'</h3>
                    <p>April 24, 2016 | 12 Baishakh, 2073 (13:00 NPT | 2nd quarter final)</p>
                    <i>- tournament match 14</i>
                  </div>

                  <div class="one-fix">
                    <h3>Winner of 'C' <span>v/s</span> Runner up of 'A'</h3>
                    <p>April 25, 2016 | 13 Baishakh, 2073 (09:00 NPT | 3rd quarter final)</p>
                    <i>- tournament match 15</i>
                  </div>

                  <div class="one-fix">
                    <h3>Winner of 'D' <span>v/s</span> Runner up of 'B'</h3>
                    <p>April 25, 2016 | 13 Baishakh, 2073 (13:00 NPT | 4th quarter final)</p>
                    <i>- tournament match 16</i>
                  </div>

                  <div class="one-fix">
                    <h3>Winner of 1st QF <span>v/s</span> Winner of 2nd QF</h3>
                    <p>April 27, 2016 | 15 Baishakh, 2073 (09:00 NPT | 1st semi final)</p>
                    <i>- tournament match 17</i>
                  </div>

                   <div class="one-fix">
                    <h3>Winner of 3rd QF <span>v/s</span> Winner of 4th QF</h3>
                    <p>April 27, 2016 | 15 Baishakh, 2073 (13:00 NPT | 2nd semi final)</p>
                    <i>- tournament match 18</i>
                  </div>

                   <div class="one-fix">
                    <h3>Winner of 1st SF <span>v/s</span> Winner of 2nd SF</h3>
                    <p>April 29, 2016 | 17 Baishakh, 2073 (10:00 NPT | Final)</p>
                    <i>- tournament match 19</i>
                  </div>
                  
                  </div>
                  
                </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="results">
                <div class="back-grey">
                <div class="total-flexboxl">
                  <div class="one-result">
                    <a href="score.html" target="_blank"><h2>match 1: ascol <span>v/s</span> Prime College<br>- <i> 05 Baishakh, 2073</i></h2></a>
                    <p>xyz won by n/a wickets (with m/a balls remaining)</p>
                  </div>
                  <div class="one-result">
                    <a href="score.html" target="_blank"><h2>match 2: ascol <span>v/s</span> Prime College<br>- <i> 05 Baishakh, 2073</i></h2></a>
                    <p>abc won by n/a runs</p>
                  </div>
                  <div class="one-result">
                    <a href="score.html" target="_blank"><h2>match 3: ascol <span>v/s</span> Prime College<br>- <i> 05 Baishakh, 2073</i></h2></a>
                    <p>pqr won by n/a wickets (with n/a balls remaining)</p>
                  </div>
                </div>
                </div>
                </div>
              </div>
          </div>
          <div class="col-md-4 col-sm-12 col-xs-12 right-stuff">
            <section id="slider1"><!--slider-->
                    <div id="slider-carousel1" class="carousel slide" data-ride="carousel" data-interval="20000">
                      <ol class="carousel-indicators">
                        <li data-target="#slider-carousel1" data-slide-to="0" class="active"></li>
                        <li data-target="#slider-carousel1" data-slide-to="1"></li>
                        <li data-target="#slider-carousel1" data-slide-to="2"></li>
                        <li data-target="#slider-carousel1" data-slide-to="3"></li>
                      </ol>
                      
                      <div class="carousel-inner">
                        <div class="item active">
                          <div class="col-sm-12">
                          <div class="heading">
                          <h3>Group 1</h3>
                          </div>
                          <table>
                              <tr>
                                <td><h4>Teams</h4></td>
                                <td><h4>gp</h4></td>
                                <td><h4>won</h4></td>
                                <td><h4>lost</h4></td>
                                <td><h4>points</h4></td>
                              </tr>
                              <tr>
                                <td>Ascol</td>
                                <td>0</td>
                                <td>0</td>
                                <td>0</td>
                                <td>0.00</td>
                              </tr>
                              <tr>
                              <tr>
                                <td>Prime College</td>
                                <td>0</td>
                                <td>0</td>
                                <td>0</td>
                                <td>0.00</td></tr>
                              <tr>
                              <tr>
                                <td>Sambridhhi</td>
                                <td>0</td>
                                <td>0</td>
                                <td>0</td>
                                <td>0.00</td></tr>
                              <tr>
                              
                          </table>
                          </div>
                        </div> 

                        <div class="item">
                          <div class="col-sm-12">
                          <div class="heading">
                          <h3>Group 2</h3>
                          </div>
                          <table>
                            <thead>
                              <tr>
                                <td><h4>Teams</h4></td>
                                <td><h4>gp</h4></td>
                                <td><h4>won</h4></td>
                                <td><h4>lost</h4></td>
                                <td><h4>points</h4></td>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>Bernhardt</td>
                                <td>0</td>
                                <td>0</td>
                                <td>0</td>
                                <td>0.00</td>
                              </tr>
                              <tr>
                              <tr>
                                <td>New Summit</td>
                                <td>0</td>
                                <td>0</td>
                                <td>0</td>
                                <td>0.00</td></tr>
                              <tr>
                              <tr>
                                <td>Orchid</td>
                                <td>0</td>
                                <td>0</td>
                                <td>0</td>
                                <td>0.00</td></tr>
                              <tr>
                             
                            </tbody>
                          </table>
                         
                          </div>
                        </div>
                        
                        <div class="item">
                          <div class="col-sm-12">
                          <div class="heading">
                          <h3>Group 3</h3>
                          </div>
                          <table>
                            <thead>
                              <tr>
                                <td><h4>Teams</h4></td>
                                <td><h4>gp</h4></td>
                                <td><h4>won</h4></td>
                                <td><h4>lost</h4></td>
                                <td><h4>points</h4></td>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>Patan</td>
                                <td>0</td>
                                <td>0</td>
                                <td>0</td>
                                <td>0.00</td>
                              </tr>
                              <tr>
                              <tr>
                                <td>Himalaya</td>
                                <td>0</td>
                                <td>0</td>
                                <td>0</td>
                                <td>0.00</td></tr>
                              <tr>
                              <tr>
                                <td>Texas</td>
                                <td>0</td>
                                <td>0</td>
                                <td>0</td>
                                <td>0.00</td></tr>
                              <tr>
                             
                            </tbody>
                          </table>
                          
                          </div>
                        </div>

                         <div class="item">
                          <div class="col-sm-12">
                          <div class="heading">
                          <h3>Group 4</h3>
                          </div>
                          <table>
                            <thead>
                              <tr>
                                <td><h4>Teams</h4></td>
                                <td><h4>gp</h4></td>
                                <td><h4>won</h4></td>
                                <td><h4>lost</h4></td>
                                <td><h4>points</h4></td>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>Henryford</td>
                                <td>0</td>
                                <td>0</td>
                                <td>0</td>
                                <td>0.00</td>
                              </tr>
                              <tr>
                              <tr>
                                <td>CAB</td>
                                <td>0</td>
                                <td>0</td>
                                <td>0</td>
                                <td>0.00</td></tr>
                              <tr>
                              <tr>
                                <td>Sagarmatha</td>
                                <td>0</td>
                                <td>0</td>
                                <td>0</td>
                                <td>0.00</td></tr>
                              <tr>
                              
                            </tbody>
                          </table>
                          
                          </div>
                        </div>
                        
                      </div>
                      <div class="carousel-controls">
                      <a href="#slider-carousel1" class="left control-carousel" data-slide="prev">
                        <i class="fa fa-angle-double-left"></i>
                      </a>
                      <a href="#slider-carousel1" class="right control-carousel" data-slide="next">
                        <i class="fa fa-angle-double-right"></i>
                      </a>
                      </div>
                    </div>
            </section><!--/slider-->

            <section class="iframe">
              <div class="fb-page" style="margin: 0 auto!important;" data-href="https://www.facebook.com/csitcricket/" data-tabs="profile" data-width="500" data-height="300" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/csitcricket/"><a href="https://www.facebook.com/csitcricket/">CSIT Inter College T20 Cricket Tournament</a></blockquote></div></div>
            </section>

          </div>
        </div>
      </div>
    </section>  

<?php include('includes/footer.php'); ?>
