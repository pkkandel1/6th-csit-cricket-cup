<!DOCTYPE html>
<html lang="en, np">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Home | Texas 6th CSIT T20 Cricket Tournament</title>

    <!-- The sass -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/home/css/style.css">

    <!-- Responsive css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/home/css/responsive.css">

    <!-- Animate css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/home/css/animate.css">

    <!-- Hover css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/home/css/hover-min.css">

    <!-- Font awesome -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/home/font-aw/css/font-awesome.min.css">

    <!-- Bootstrap -->
    <link href="<?php echo base_url();?>assets/home/css/bootstrap.min.css" rel="stylesheet">

    <!-- Roboto Font -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:100,400,700,300' rel='stylesheet' type='text/css'>

    <!-- Cursive -->
    <link href='https://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>

    <!-- Owlcarousel -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/home/owlcarousel/assets/owl.carousel.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https:k//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
   
    <div id="fb-root" style="display: none;"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.4";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    </script>

  <!-- The navigation -->
    <nav class="navbar navbar-default">
        <div class="container-fluid">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <div class="logo"><img src="<?php echo base_url();?>assets/home/images/logo.png" class="img-responsive"></div>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse pull-right" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
              <li><a href="<?php echo base_url();?>home" class="active"><i class="fa fa-home"></i> home</a></li>
              <li><a href="<?php echo base_url();?>home/#tabs" ><i class="fa fa-newspaper-o"></i> News</a></li>
              <li><a href="<?php echo base_url();?>home/#tabs" ><i class="fa fa-users"></i> Teams</a></li>
              <li><a href="<?php echo base_url();?>home/#tabs" ><i class="fa fa-calendar-o"></i> Fixtures</a></li>
              <li><a href="<?php echo base_url();?>home/#tabs" ><i class="fa fa-table"></i> Result</a></li>
              <!-- <li><a href="score.html"><i class="fa fa-list-ul"></i> Recent Result</a></li> -->
            </ul>
          </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
