  <!-- Footer Stuff -->

    <section class="footer">
      <footer>
        <div class="container">

        <div class="top-two">

          <div class="box-logos">
            <div class="box">
              <span>organiser</span>
            </div>
            <div class="image">
            <img src="<?php echo base_url();?>assets/home/images/ascol.png" alt="Logo" class="img-responsive" title="Amrit Science Campus">
            </div>
          </div>

          <div class="box-logos">
            <div class="box">
              <span>title sponser</span>
            </div>
            <div class="image">
            <img src="<?php echo base_url();?>assets/home/images/texas.png" alt="Logo" class="img-responsive" title="Texas International College">
            </div>
          </div>

        
          </div>

          <div class="bottom-one">

          <div class="box-logos">
            <div class="box">
              <span>media partners</span>
            </div>
            <div class="image">
            <img src="<?php echo base_url();?>assets/home/images/him2.jpg" alt="Logo" class="img-responsive him">
            <img src="<?php echo base_url();?>assets/home/images/Gorkhapatra.jpg" alt="Logo" class="img-responsive">
            </div>
          </div>

          </div>

        </div>

        <div class="footer-bottom">
          copyright <i class="fa fa-copyright"></i> Csit batch 2070 Ascol. 2073
        </div>

      </footer>
    </section>     
   

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url();?>assets/home/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/home/owlcarousel/owl.carousel.min.js"></script>
    <script src="<?php echo base_url();?>assets/home/js/responsive-tabs.js"></script>
    <script src="<?php echo base_url();?>assets/home/js/raphael-min.js"></script>
    <script src="<?php echo base_url();?>assets/home/js/livicons-1.3.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/home/js/swipe.js"></script>

    <script type="text/javascript">
      (function($) {
          fakewaffle.responsiveTabs(['xs']);
      })(jQuery);
    </script>

    <script type="text/javascript">
     
        $(document).ready(function() {  
             $("#slider-carousel, #slider-carousel1").swiperight(function() {  
                $(this).carousel('prev');  
                });  
             $("#slider-carousel, #slider-carousel1").swipeleft(function() {  
                $(this).carousel('next');  
           });  
        });  
    </script>



  </body>
</html>