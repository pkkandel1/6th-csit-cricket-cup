<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="title" content="<?php if(!empty($title)) {echo $title;} ?>">
        <meta name="keywords" content="<?php  if(!empty($meta_keywords)) {echo $meta_title;} ?>">
        <meta name="description" content="<?php if(!empty($metadescription)){echo $meta_description; }?>">
        <title><?php if (!empty($title)) {
    echo $title;
} else {
    echo"Kamyak school";
} ?></title>
        <!-- <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,700,500,900,300italic,300' rel='stylesheet' type='text/css'> -->
        <!-- Bootstrap -->
        <link type="text/css" href="<?php echo base_url(); ?>assets/home/css/bootstrap.css" rel="stylesheet">
        <link type="text/css" href="<?php echo base_url(); ?>assets/home/css/style.css" rel="stylesheet">
        <!----fonts----------->
        <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/home/fonts/fa/css/font-awesome.min.css" />  

    </head>
    <body>
    <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

        <div class="row row-header" style="position:relative;">
            <div class="row-header-detail">
                <div class="col-md-3 col-sm-12 col-xs-12 row-header-detail-left">
                    <img src="<?php echo base_url(); ?>assets/home/images/logo.png" class="img-responsive">
                </div>
                <div class="col-md-9 col-sm-12 col-xs-12 row-header-detail-right">
                    <div class="header-right">
                        Phone: +977 14255124,   4245571   |  <a href="#">  E-mail: kamyakschool@gmail.com</a>
                    </div>

                    <div class="header-menu-right">
                        <nav class="navbar navbar-default">
                            <div class="container-fluid">
                                <!-- Brand and toggle get grouped for better mobile display -->
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>

                                </div>

                                <!-- Collect the nav links, forms, and other content for toggling -->
                                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                    <ul class="nav navbar-nav">

                                        <li class="dropdown">

                                            <a href="<?php echo base_url(); ?>" >HOME </span></a>
                                            <!-----  <ul class="dropdown-menu" role="menu">
                                                    <li><a href="#">Action</a></li>
                                                    <li><a href="#">Another action</a></li>
                                                    <li><a href="#">Something else here</a></li>
                                                    <li class="divider"></li>
                                                    <li><a href="#">Separated link</a></li>
                                                    <li class="divider"></li>
                                                    <li><a href="#">One more separated link</a></li>
                                              </ul>------->
                                        </li>

                                        <li>
                                            <a href="<?php echo base_url(); ?>pages/about-us" >ABOUT</a>
                                            <!-----  <ul class="dropdown-menu" role="menu">
                                                    <li><a href="#">Action</a></li>
                                                    <li><a href="#">Another action</a></li>
                                                    <li><a href="#">Something else here</a></li>
                                                    <li class="divider"></li>
                                                    <li><a href="#">Separated link</a></li>
                                                    <li class="divider"></li>
                                                    <li><a href="#">One more separated link</a></li>
                                              </ul>------->
                                        </li>


                                        <li class="dropdown">
                                            <a href="<?php echo base_url(); ?>news" >NEWS </span></a>
                                            <!-----  <ul class="dropdown-menu" role="menu">
                                                    <li><a href="#">Action</a></li>
                                                    <li><a href="#">Another action</a></li>
                                                    <li><a href="#">Something else here</a></li>
                                                    <li class="divider"></li>
                                                    <li><a href="#">Separated link</a></li>
                                                    <li class="divider"></li>
                                                    <li><a href="#">One more separated link</a></li>
                                              </ul>------->
                                        </li>


                                        <li class="dropdown">
                                            <a href="<?php echo base_url(); ?>home/gallery" >GALLERY </span></a>
                                            <!-----  <ul class="dropdown-menu" role="menu">
                                                    <li><a href="#">Action</a></li>
                                                    <li><a href="#">Another action</a></li>
                                                    <li><a href="#">Something else here</a></li>
                                                    <li class="divider"></li>
                                                    <li><a href="#">Separated link</a></li>
                                                    <li class="divider"></li>
                                                    <li><a href="#">One more separated link</a></li>
                                              </ul>------->
                                        </li>


                                        <li class="dropdown">
                                            <a href="<?php echo base_url(); ?>contact" >CONTACT</span></a>
                                            <!-----  <ul class="dropdown-menu" role="menu">
                                                    <li><a href="#">Action</a></li>
                                                    <li><a href="#">Another action</a></li>
                                                    <li><a href="#">Something else here</a></li>
                                                    <li class="divider"></li>
                                                    <li><a href="#">Separated link</a></li>
                                                    <li class="divider"></li>
                                                    <li><a href="#">One more separated link</a></li>
                                              </ul>------->
                                        </li>

                                    </ul>


                                </div><!-- /.navbar-collapse -->
                            </div><!-- /.container-fluid -->
                        </nav>

                          <div class="search">
                            <form action="<?php echo site_url('home/search');?>" class="search-form">
                                <div class="form-group has-feedback">
                                    <label for="search" class="sr-only">Search</label>
                                    <input type="text" class="form-control" name="search" id="search" placeholder="search">
                                    <span class="glyphicon glyphicon-search form-control-feedback"></span>
                                </div>
                            </form>
                        </div>

                    </div><!--end of header menu right-->						
                </div>
            </div>
        </div><!--row-header-->