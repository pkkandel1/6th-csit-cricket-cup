<div class="row row-footer" data-type="background" data-speed="20">
    <div class="row-footer-detail">
        <div class="col-md-2 col-sm-6 col-xs-6 row-footer-detail-info"> 
            <h3>Site Map</h3>
            <li><a href="<?php echo base_url(); ?>">HOME</a></li>
            <li><a href="<?php echo base_url(); ?>pages/about-us">ABOUT US</a></li>
            <li><a href="<?php echo base_url(); ?>news">NEWS AND EVENTS</a></li>
             <li><a href="<?php echo base_url(); ?>services">SERVICES</a></li>
            <li><a href="<?php echo base_url(); ?>/home/gallery">GALLERY</a></li>
            <li><a href="<?php echo base_url(); ?>contact">CONTACT US</a></li>
            <li><a href="#">CHECK YOUR MAIL</a></li>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-6 row-footer-detail-info">
            <h3>Why us</h3>
            <p>This is Photoshop's version  of Lorem Ipsum.  Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit.<br><br>This is Photoshop's version  of Lorem Ipsum.Proin gravida nibh vel velit auctor aliquet.</p>
        </div>

       <div class="col-md-3 col-sm-6 col-xs-6 row-footer-detail-info"> 
            <h3>Contact</h3>
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3532.376544937024!2d85.31969600000001!3d27.705657999999985!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb1907593ec2f9%3A0xbeb329c5228b0cdc!2sKamyak+School!5e0!3m2!1sen!2snp!4v1427964341712" width="250" height="164" frameborder="0" style="border:0"></iframe>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-6 row-footer-detail-info" id="sucessMsg">
            <form method="post"  id="myform">
                <div class="form-group">

                   <input type="text" class="form-control" id="exampleInputName" placeholder="Name" name="name" >
                    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email" name="email" >
                    <textarea class="form-control" rows="3" id="exampleInputmessage" placeholder="Message" name="message"></textarea>
                    <button class="btn btn-default btn-default-one" id="submit" type="submit">Submit</button>
                </div>
            </form>	  
        </div>
    </div>
</div><!--end of row-footer-->

<div class="row row-copyright">
    <p>Copyright @ <a href="makuracreations.com" target="_blank">Makura Creations</a></p>
</div>
<script src="<?php echo base_url(); ?>assets/home/js/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?php echo base_url(); ?>assets/home/js/bootstrap.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $("#myform").submit(function() {
        var fn=document.getElementById('exampleInputName').value;
    if(fn == ""){
        alert('Please Enter First Name');
        document.getElementById('exampleInputName').style.borderColor = "red";
        return false;
    }else{
        document.getElementById('exampleInputName').style.borderColor = "green";
    }
	
	var email=document.getElementById('exampleInputEmail1').value;
    if(email == ""){
        alert('Please Enter valid email');
        document.getElementById('exampleInputEmail1').style.borderColor = "red";
        return false;
    }else{
        document.getElementById('exampleInputEmail1').style.borderColor = "green";
    }
	var pattern=/^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/;
    if(pattern.test(email)){         
		document.getElementById('exampleInputEmail1').style.borderColor = "green";  
    }else{   
		alert("enter valid  email "); 
		return false;
    }
	
	
	
	var fn=document.getElementById('exampleInputmessage').value;
    if(fn == ""){
        alert('Please Enter message');
        document.getElementById('exampleInputmessage').style.borderColor = "red";
        return false;
    }else{
        document.getElementById('exampleInputmessage').style.borderColor = "green";
    }
	
            var data=$(this).serialize();
            
//            alert(data);return false;
          $.ajax({
           type:'post',
           url:'<?php echo base_url();?>home/sendMail',
           data:data,
           datatype:'json',
           success:function(res){
       var data = JSON.parse(res);
      if(data.type=="success"){
          alert(data.msg);
           }
           else{
               alert(data.msg);
           }
               }
          });
          return false;
    });
    });
</script>

<script>$(document).ready(function() {
        //Set the carousel options
        $('#quote-carousel').carousel({
            pause: true,
            interval: 4000,
        });
    });</script>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

</body>
</html>