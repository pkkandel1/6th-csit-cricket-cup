c<?php include 'includes/otherheader.php'; ?>


<div class="clear"></div>

<div class="row row-top-image" data-type="background" data-speed="20">
    <div class="row-top-image-detail">
        <button class="btn btn-default btn-default-one" type="submit"><?php echo $page->name; ?></button>
    </div>	
</div>

<div class="wrapper">
    <div class="col-md-8 col-sm-12 col-xs-12 content-left">
        <h3><?php echo $page->name; ?></h3>
	
		<p> <?php if(!empty($page->image))
		{ ?>
		<img class="img-responsive" src="<?php echo base_url().'uploads/page_images/thumb/'.$page->image ;?>">
		
		<?php } ?>
		</p>
        <p>
		
			<?php 
			$str =  $page->description; 
			$search     = array('$#');
			$clean      = str_replace($search, ' ', $str); 
			echo $clean;
		?>
		</p>

       
    </div><!--end of content-left--->
    <?php include('includes/rightsidebar.php'); ?>

    <div class="col-md-12 row row-gallery">
       <div id="Carousel" class="carousel slide">

            <ol class="carousel-indicators">
                <li data-target="#Carousel" data-slide-to="0" class="active"></li>
                <li data-target="#Carousel" data-slide-to="1"></li>
                <li data-target="#Carousel" data-slide-to="2"></li>
            </ol>

            <!-- Carousel items -->
            <div class="carousel-inner">
                <?php
				$count = count($images);
				$div_count = 0;
				for($i=1;$i<=$count;$i++){
				if($i%4 == 0){
					$sliced_images = array_slice($images,$i-4, 4);
				?>
				<div class="<?php
				if($div_count == 0){
					echo 'item active';
				}else{
					echo 'item';
				}
				?>">
                    <div class="row">
                        <div class="slider-image">
						<?php foreach($sliced_images as $im){?>
							<div class="col-md-3"><a href="#" class="thumbnail"><img src="<?php echo base_url().'uploads/gallery_images/resized/'.$im->image ;?>" alt="Image" style="max-width:100%;"></a></div>
						<?php $div_count++;} ?>
						</div>
                    </div>
                </div>
						<?php
					}
				}
				?>
				
            </div>

            <a data-slide="prev" href="#Carousel" class="left carousel-control carousel-control-footer ">‹</a>
            <a data-slide="next" href="#Carousel" class="right carousel-control carousel-control-footer">›</a>
        </div><!--.Carousel-->
    </div><!--end of row-gallery-->

</div><!--end of wrapper-->
<div class="clear"></div>


<div class="clear"></div>

<?php include 'includes/otherfooter.php'; ?>