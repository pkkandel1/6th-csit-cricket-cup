<?php include('includes/header.php');?>

<div class="container">
	<div class="row">
		<!-- Slider -->
		<section class="room-slider standard-slider mt50">
			<div class="col-sm-12 col-md-8">
					<div class="item"> <a href="<?php echo base_url();?>assets/home/images/rooms/slider/room-slider-02.jpg" data-rel="prettyPhoto[gallery1]"><img src="<?php echo base_url();?>assets/home/images/rooms/slider/room-slider-02.jpg" alt="Bed" class="img-responsive"></a> </div>
			</div>
		</section>

		<!-- Reservation form -->
		<section id="reservation-form" class="mt50 clearfix">
			<div class="col-sm-12 col-md-4">
				<form class="reservation-vertical clearfix" role="form" method="post" action="" name="reservationform" id="reservationform">
					<h2 class="lined-heading"><span>Search packages</span></h2>

					<div id="message"></div>
					<!-- Error message display -->
					<div class="form-group">
						<label for="email" accesskey="E">E-mail</label>
						<input name="email" type="text" id="email" value="" class="form-control" placeholder="Please enter your E-mail"/>
					</div>
					<div class="form-group">
						<select class="hidden" name="room" id="room" disabled="disabled">
							<option selected="selected">Double Room</option>
						</select>
					</div>
					<div class="form-group">
						<label for="checkin">Check-in</label>
						<div class="popover-icon" data-container="body" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="Check-In is from 11:00"> <i class="fa fa-info-circle fa-lg"> </i> </div>
						<i class="fa fa-calendar infield"></i>
						<input name="checkin" type="text" id="checkin" value="" class="form-control" placeholder="Check-in"/>
					</div>
					<div class="form-group">
						<label for="checkout">Check-out</label>
						<div class="popover-icon" data-container="body" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="Check-out is from 12:00"> <i class="fa fa-info-circle fa-lg"> </i> </div>
						<i class="fa fa-calendar infield"></i>
						<input name="checkout" type="text" id="checkout" value="" class="form-control" placeholder="Check-out"/>
					</div>
					<div class="form-group">
						<div class="guests-select">
							<label>Guests</label>
							<i class="fa fa-user infield"></i>
							<div class="total form-control" id="test">1</div>
							<div class="guests">
								<div class="form-group adults">
									<label for="adults">Adults</label>
									<div class="popover-icon" data-container="body" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="+18 years"> <i class="fa fa-info-circle fa-lg"> </i> </div>
									<select name="adults" id="adults" class="form-control">
										<option value="1">1 adult</option>
										<option value="2">2 adults</option>
										<option value="3">3 adults</option>
									</select>
								</div>
								<div class="form-group children">
									<label for="children">Children</label>
									<div class="popover-icon" data-container="body" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="0 till 18 years"> <i class="fa fa-info-circle fa-lg"> </i> </div>
									<select name="children" id="children" class="form-control">
										<option value="0">0 children</option>
										<option value="1">1 child</option>
										<option value="2">2 children</option>
										<option value="3">3 children</option>
									</select>
								</div>
								<button type="button" class="btn btn-default button-save btn-block">Save</button>
							</div>
						</div>
					</div>
					<button type="submit" class="btn btn-primary btn-block">Book Now</button>
				</form>
			</div>
		</section>

		<!-- Room Content -->
		<section>
			<div class="container">
				<div class="row">
					<div class="col-sm-7 mt50">
						<h2 class="lined-heading"><span>Know more about us</span></h2>

						<p class="mt50">Quisque at consectetur neque. Donec rhoncus sagittis sem, ac tempus erat blandit nec! Sed gravida purus vitae eros aliquam imperdiet. Curabitur feugiat risus in tellus convallis tempor. Donec lobortis rhoncus ullamcorper. Mauris leo dolor, fringilla at aliquet ac, tincidunt nec felis. Aenean rhoncus iaculis dignissim. Aliquam erat volutpat. Phasellus vitae nisi cursus, hendrerit enim non, faucibus diam.</p>

						<p class="mt50">Nullam orci elit, convallis mattis dui nec; aliquam vestibulum nibh. Quisque in lectus eu orci facilisis congue? Aenean fringilla ex eget felis aliquam consequat. Donec tempus arcu eu quam fermentum venenatis. Nulla iaculis efficitur est. Praesent consequat erat erat, sit amet laoreet neque varius non. Quisque eget aliquam erat, eget tristique nulla. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Fusce egestas lorem id orci ultrices lobortis. Quisque dictum tortor non orci tempus lobortis. Vivamus sagittis vel urna ut tristique. Proin faucibus, massa eget eleifend eleifend, ligula erat posuere lorem, vel viverra lorem quam in risus. Nulla non semper tellus.</p>

						<p class="mt50">Vivamus eu velit est. In non consectetur arcu! In hac habitasse platea dictumst. Nam pretium purus vitae gravida pretium. Sed tempor turpis lacus, vitae interdum nulla imperdiet lacinia. Donec mauris elit, tempor nec feugiat sit amet, lobortis sit amet urna. Aliquam laoreet feugiat vehicula.</p>
						</p>
					</div>
					<div class="col-sm-5 mt50">
						<h2 class="lined-heading"><span>Overview</span></h2>

						<!-- Nav tabs -->
						<ul class="nav nav-tabs">
							<li class="active"><a href="#overview" data-toggle="tab">Our Team</a></li>
							<li><a href="#facilities" data-toggle="tab">Our Facilities</a></li>
						</ul>
						<!-- Tab panes -->
						<div class="tab-content">
							<div class="tab-pane fade in active" id="overview">
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse interdum eleifend augue, quis rhoncus purus fermentum. In hendrerit risus arcu, in eleifend metus dapibus varius. Nulla dolor sapien, laoreet vel tincidunt non, egestas non justo. Phasellus et mattis lectus, et gravida urna.</p>
								<p><img src="<?php echo base_url();?>assets/home/images/tab/restaurant-01.jpg" alt="food" class="pull-right"> Donec pretium sem non tincidunt iaculis. Nunc at pharetra eros, a varius leo. Mauris id hendrerit justo. Mauris egestas magna vitae nisi ultricies semper. Nam vitae suscipit magna. Nam et felis nulla. Ut nec magna tortor. Nulla dolor sapien, laoreet vel tincidunt non, egestas non justo. </p>
							</div>
							<div class="tab-pane fade" id="facilities">Phasellus sodales justo felis, a vestibulum risus mattis vitae. Aliquam vitae varius elit, non facilisis massa. Vestibulum luctus diam mollis gravida bibendum. Aliquam mattis velit dolor, sit amet semper erat auctor vel. Integer auctor in dui ac vehicula. Integer fermentum nunc ut arcu feugiat, nec placerat nunc tincidunt. Pellentesque in massa eu augue placerat cursus sed quis magna.</div>

						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</div>

<!-- Other Rooms -->
<section class="rooms mt50">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h2 class="lined-heading"><span>Packages we offer</span></h2>
			</div>
			<!-- Room -->
			<div class="col-sm-4">
				<div class="room-thumb"> <img src="<?php echo base_url();?>assets/home/images/rooms/room-01.jpg" alt="room 1" class="img-responsive" />
					<div class="mask">
						<div class="main">
							<h5>Double bedroom</h5>
							<div class="price">&euro; 99<span>a night</span></div>
						</div>
						<div class="content">
							<p><span>A modern hotel room in Star Hotel</span> Nunc tempor erat in magna pulvinar fermentum. Pellentesque scelerisque at leo nec vestibulum.
								malesuada metus.</p>
							<div class="row">
								<div class="col-xs-6">
									<ul class="list-unstyled">
										<li><i class="fa fa-check-circle"></i> Incl. breakfast</li>
										<li><i class="fa fa-check-circle"></i> Private balcony</li>
										<li><i class="fa fa-check-circle"></i> Sea view</li>
									</ul>
								</div>
								<div class="col-xs-6">
									<ul class="list-unstyled">
										<li><i class="fa fa-check-circle"></i> Free Wi-Fi</li>
										<li><i class="fa fa-check-circle"></i> Incl. breakfast</li>
										<li><i class="fa fa-check-circle"></i> Bathroom</li>
									</ul>
								</div>
							</div>
							<a href="room-detail.html" class="btn btn-primary btn-block">Book Now</a> </div>
					</div>
				</div>
			</div>
			<!-- Room -->
			<div class="col-sm-4">
				<div class="room-thumb"> <img src="<?php echo base_url();?>assets/home/images/rooms/room-02.jpg" alt="room 2" class="img-responsive" />
					<div class="mask">
						<div class="main">
							<h5>King Size Bedroom </h5>
							<div class="price">&euro; 149<span>a night</span></div>
						</div>
						<div class="content">
							<p><span>A modern hotel room in Star Hotel</span> Nunc tempor erat in magna pulvinar fermentum. Pellentesque scelerisque at leo nec vestibulum.
								malesuada metus.</p>
							<div class="row">
								<div class="col-xs-6">
									<ul class="list-unstyled">
										<li><i class="fa fa-check-circle"></i> Incl. breakfast</li>
										<li><i class="fa fa-check-circle"></i> Private balcony</li>
										<li><i class="fa fa-check-circle"></i> Sea view</li>
									</ul>
								</div>
								<div class="col-xs-6">
									<ul class="list-unstyled">
										<li><i class="fa fa-check-circle"></i> Free Wi-Fi</li>
										<li><i class="fa fa-check-circle"></i> Incl. breakfast</li>
										<li><i class="fa fa-check-circle"></i> Bathroom</li>
									</ul>
								</div>
							</div>
							<a href="room-detail.html" class="btn btn-primary btn-block">Book Now</a> </div>
					</div>
				</div>
			</div>
			<!-- Room -->
			<div class="col-sm-4">
				<div class="room-thumb"> <img src="<?php echo base_url();?>assets/home/images/rooms/room-03.jpg" alt="room 3" class="img-responsive" />
					<div class="mask">
						<div class="main">
							<h5>Single room</h5>
							<div class="price">&euro; 120<span>a night</span></div>
						</div>
						<div class="content">
							<p><span>A modern hotel room in Star Hotel</span> Nunc tempor erat in magna pulvinar fermentum. Pellentesque scelerisque at leo nec vestibulum.
								malesuada metus.</p>
							<div class="row">
								<div class="col-xs-6">
									<ul class="list-unstyled">
										<li><i class="fa fa-check-circle"></i> Incl. breakfast</li>
										<li><i class="fa fa-check-circle"></i> Private balcony</li>
										<li><i class="fa fa-check-circle"></i> Sea view</li>
									</ul>
								</div>
								<div class="col-xs-6">
									<ul class="list-unstyled">
										<li><i class="fa fa-check-circle"></i> Free Wi-Fi</li>
										<li><i class="fa fa-check-circle"></i> Incl. breakfast</li>
										<li><i class="fa fa-check-circle"></i> Bathroom</li>
									</ul>
								</div>
							</div>
							<a href="room-detail.html" class="btn btn-primary btn-block">Book Now</a> </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Footer -->
<!-- Footer -->
<?php include('includes/footer.php');?>