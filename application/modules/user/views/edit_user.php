<ol class="breadcrumb bc-3">
    <li>
        <a href="index.html"><i class="entypo-home"></i>Home</a>
    </li>
    <li>

        <a href="forms-main.html">User</a>
    </li>
    <li class="active">
       

        <strong>Add</strong>
    </li>
</ol>
<?php
$role = $this->session->userdata('role');
if($role=="admin"){
?>
<a href="<?php echo base_url(); ?>user/all" class="btn btn-blue">
    <i class="entypo-search"></i>
    View Users
</a>
<?php } ?>
<h2>Add User</h2>
<br />

<div class="panel panel-primary">

    <div class="panel-heading">
        <div class="panel-title">All fields have validation rules <small></small></div>

        <div class="panel-options">
            <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i class="entypo-cog"></i></a>
            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
            <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
            <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
        </div>
    </div>

    <div class="panel-body">
        <?php if (!empty($message)) { ?>
            <div class="col-md-12">
                <div class="alert alert-danger"><?php echo $message; ?></div>
            </div>
        <?php } ?>

        <form role="form" id="form1" method="post" class="validate" action="<?php echo base_url(); ?>user/edit/<?php echo $user->id;?>" enctype="multipart/form-data">

            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">First Name</label>

                    <input type="text" class="form-control" name="first_name" data-validate="required" data-message-required="This field is required" placeholder="first name" value="<?php
                    if (!empty($user)) {
                        echo $user->first_name;
                    }
                    ?>"/>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Last Name</label>

                    <input type="text" class="form-control" name="last_name" data-validate="required" data-message-required="This field is required" placeholder="Last Name" value="<?php
                    if (!empty($user)) {
                        echo $user->last_name;
                    }
                    ?>"/>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Email</label>

                    <input type="text" class="form-control" name="email" data-validate="required,email" data-message-required="This field is required" placeholder="Email " value="<?php
                    if (!empty($user)) {
                        echo $user->email;
                    }
                    ?>"/>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Phone</label>

                    <input type="text" class="form-control" name="phone" data-validate="required,phone" data-message-required="This field is required" placeholder="Phone" value="<?php
                    if (!empty($user)) {
                        echo $user->phone;
                    }
                    ?>"/>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Password</label>

                    <input type="password" class="form-control" name="password" data-validate="" data-message-required="This field is required" placeholder="password"/>
                </div>

            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Confirm Password</label>

                    <input type="password" class="form-control" name="password_confirm" data-validate="" data-message-required="This field is required" placeholder="Confirm Password" />
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <!--<label class="control-label">User Image</label>-->
                    <label for="name">Current User image</label>
                    <img src="<?php echo base_url(); ?>uploads/user_images/thumb/<?php echo $user->image; ?>">
                </div>
            </div>
            
            
            <div class="col-md-6">
                <div class="form-group">
                    <!--<label class="control-label">User Image</label>-->

                    <input type="file" name="userfile" class="form-control file2 inline btn btn-primary" data-label="<i class='glyphicon glyphicon-file'></i> Upload new user image" />
                </div>
            </div>
            

            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Confirm Password</label>

                    <?php foreach ($groups as $group): ?>
                        <label class="checkbox">
                            <?php
                            $gID = $group['id'];
                            $checked = null;
                            $item = null;
                            foreach ($currentGroups as $grp) {
                                if ($gID == $grp->id) {
                                    $checked = ' checked="checked"';
                                    break;
                                }
                            }
                            ?>
                            <input type="checkbox" name="groups[]" value="<?php echo $group['id']; ?>"<?php echo $checked; ?>>
                            <?php echo $group['name']; ?>
                        </label>
                    <?php endforeach ?>
                </div>
            </div>



            <?php echo form_hidden('id', $user->id); ?>





            <div class="clearfix"></div>


            <div class="col-md-6">
                <div class="form-group">
                    <button type="submit" class="btn btn-success" name="submit">Submit</button>
                    <button type="reset" class="btn">Reset</button>
                </div>
            </div>

        </form>

    </div>

</div><!-- Footer -->
