
<body class="page-body login-page login-form-fall">


    <!-- This is needed when you send requests via Ajax --><script type="text/javascript">
        var baseurl = "<?php echo base_url(); ?>";
    </script>

    <div class="login-container">

        <div class="login-header login-caret">

            <div class="login-content">

                <h1><a href="index.html" class="logo">
                        The Last Resort
                    </a></h1>

                <p class="description">Dear user, log in to access the admin area!</p>

                <!-- progress bar indicator -->
                <div class="login-progressbar-indicator">
                    <h3>43%</h3>
                    <span>Processing ...</span>
                </div>
            </div>

        </div>

        <div class="login-progressbar">
            <div></div>
        </div>

        <div class="login-form">

            <div class="login-content">


                <?php if (!empty($message)) { ?>
                    <div class="form-login-error" style="display:block;">
                        <h3>Invalid login</h3>
                        <p><?php echo $message; ?></p>
                    </div>
                <?php } ?>

                <form method="post" role="form"  action="<?php echo base_url(); ?>user/reset_password/<?php echo $code; ?>">

                    <div class="form-group">

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="entypo-key"></i>
                            </div>

                            <input type="password" class="form-control" name="new" id="new_password" placeholder="Password" autocomplete="off" required="required"/>
                        </div>

                    </div>

                    <div class="form-group">

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="entypo-key"></i>
                            </div>

                            <input type="password" class="form-control" name="new_confirm" id="new_password_confirm" placeholder="Confirm Password" autocomplete="off" required/>
                        </div>

                    </div>

                    <?php echo form_input($user_id); ?>
                    <?php echo form_hidden($csrf); ?>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-block btn-login">
                            <i class="entypo-login"></i>
                            Login In
                        </button>
                    </div>


                </form>


                <div class="login-bottom-links">

                    <a href="<?php echo base_url(); ?>user/forgot_password" class="link">Forgot your password?</a>

                    <br />

                    <a href="#">ToS</a>  - <a href="#">Privacy Policy</a>

                </div>

            </div>

            <br/>
