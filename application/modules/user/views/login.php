<body class="page-body login-page login-form-fall">


    <!-- This is needed when you send requests via Ajax -->
    <script type="text/javascript">
        var baseurl = "<?php echo base_url(); ?>";
    </script>

    <div class="login-container">

        <div class="login-header login-caret">

            <div class="login-content">

                <h1><a href="index.html" class="logo">
                        <img src="<?php echo base_url(); ?>assets/home/images/logop.png">
                    </a></h1>

                <p class="description">Dear user, log in to access the admin area!</p>

                <!-- progress bar indicator -->
                <div class="login-progressbar-indicator">
                    <h3>43%</h3>
                    <span>logging in...</span>
                </div>
            </div>

        </div>

        <div class="login-progressbar">
            <div></div>
        </div>

        <div class="login-form">

            <div class="login-content">

                <!--			<div class="form-login-error">
                                                <h3>Invalid login</h3>
                
                                                <p>Enter <strong>demo</strong>/<strong>demo</strong> as login and password.</p>
                                        </div>-->

                <div class="form-login-error">
                    <h3>Invalid login</h3>
                    <p>Enter Valid Username or password</p>
                </div>

                <form method="post" role="form" id="form_login" action="user/login">

                    <div class="form-group">

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="entypo-user"></i>
                            </div>

                            <input type="text" class="form-control" name="username" id="username" placeholder="Username"
                                   autocomplete="on"/>
                        </div>


                    </div>


                   <!--  <?php if (!empty($message)) { ?>
                        <div class="form-login-error">
                            <h3>Notice : </h3>
                            <p><?php echo $message; ?></p>
                        </div>
                    <?php } ?> -->


                    <div class="form-group">

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="entypo-key"></i>
                            </div>

                            <input type="password" class="form-control" name="password" id="password" placeholder="Password"
                                   autocomplete="off"/>
                        </div>

                    </div>

                    <div class="form-group">
                        <input type="checkbox" class="btn btn-primary btn-login" name="remember" id="remember">
                        &nbsp;&nbsp;Remember
                        me

                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-block btn-login">
                            <i class="entypo-login"></i>
                            Login In
                        </button>
                    </div>
                </form>
                <div class="login-bottom-links">

                    <a href="<?php echo base_url(); ?>user/forgot_password" class="link">Forgot your password?</a>


                </div>

            </div>