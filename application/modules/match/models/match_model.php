<?php

class Match_model extends CI_Model {

    private $table = "tbl_match_detail";

    function construct() {
        parent::__construct();
    }

    function index() {

        $this->load->view('admin/post');
    }

    /*
     * Function to add destination from form
     */

    function manage($id='') {

        $data = array(
           'team1_id' => $this->input->post('team1_id'),
           'team2_id' => $this->input->post('team2_id'),
           'match_day' => $this->input->post('match_day'),
           'match_type' => $this->input->post('match_type'),
           'description' => $this->input->post('description'),
           'ump1' => $this->input->post('ump1'),
           'ump2' => $this->input->post('ump2'),
           'motm' => $this->input->post('motm'),
           );

        if ($id == '') {

            $this->db->insert($this->table, $data);
            $this->session->set_flashdata('message', "Insertion successfull");
        } else {           
            $this->db->where('id', $id);
            $this->db->update($this->table, $data);
            $this->session->set_flashdata('message',"Information updated successfully");
        }
    }



    function getAll() {
        $this->db->select('*');
        $this->db->from($this->table);
        $query = $this->db->get();
        return $query->result();
    }

    //  function getAll() {
    //     $query = $this->db->query("SELECT m.*,t.team_name as tname from tbl_match_detail m JOIN tbl_team_detail t on t.id = m.team1_id || t.id = m.team2_id ");
    //     return $query->result();
    // }


    function getSingle($id) {
        $data = $this->db->query("SELECT * FROM $this->table WHERE id='$id'");
        return $data->row($id);
    }
    
    function delete($id){
        $this->db->delete($this->table,array('id'=>$id));
        $this->session->set_flashdata('message',"Information Deleted successfully");        
    }

    function getPublish(){
	$data=$this->db->query("SELECT * FROM $this->table WHERE publish='1' ORDER BY published_date DESC limit 4");
	return $data->result();
    }

   

    function getOtherArticle() {
        $this->db->select('*');
    $this->db->from($this->table);
    $this->db->where('publish',1);
    $this->db->where('type','3');
    $this->db->limit(2);
    $query = $this->db->get();
    return $query->result();
    }

}

?>
