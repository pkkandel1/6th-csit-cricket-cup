<?php

class Match extends MX_Controller {

  function __construct() {
    parent::__construct();
    ob_start();
    $this->load->model('match/match_model');
    $this->load->model('team/team_model');
    if (!$this->input->is_ajax_request()) {
      modules::run('user/auth/check_login');
    }
  }

  function index() {
    $this->getAll();
  }

  


 function add() {
  if ($_POST) {
    $this->match_model->manage();
    $this->getAll();
  } else {
    $data['title'] = "Add New match";
    $data['team']=$this->team_model->getAll();
    $this->load->view('base/header', $data);
    $this->load->view('match_form');
    $this->load->view('base/footer');
  }
}

/*    Function to validate the post of the form     */

function getAll() {
  $data['title'] = "View matchs";        
  $data['all'] = $this->match_model->getAll();
  $this->load->view('base/header', $data);
  $this->load->view('match_table');
  $this->load->view('base/table_footer');
}

    /*
     * Function to update the destination 
     * id:destinaion identity  as argument 
     */

    function edit($id = '') {
//        $id = $this->uri->segment(3);
      if ($_POST) {
        $data = $this->match_model->getSingle($id);
        $this->match_model->manage($id);
        redirect('match');
      } else {
        $data['single'] = $this->match_model->getSingle($id);

        if (empty($data['single'])) {
          show_404();
        }
        $data['title'] = "Edit Match";
        $this->load->view('base/header', $data);
        $this->load->view('match_form');
        $this->load->view('base/footer');
      }
    }

    /*
     * Function to Delete New Book 
     * id:Book identity  as argument 
     */

    function delete($id) {
      $this->match_model->delete($id);
      redirect('match');
    }

  }

  ?>
