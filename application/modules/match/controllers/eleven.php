<?php

class Eleven extends MX_Controller {

  function __construct() {
    parent::__construct();
    ob_start();
    $this->load->model('match/match_model');
    $this->load->model('match/eleven_model');
    $this->load->model('team/team_model');
    $this->load->model('team/player_model');
    if (!$this->input->is_ajax_request()) {
      modules::run('user/auth/check_login');
    }
  }

  function index() {
    $this->getAll();
  }

  


 function add() {
  if ($_POST) {
    $this->eleven_model->manage();
    $this->getAll();
  } else {
    $data['title'] = "Add New Match details";
    $data['team']=$this->team_model->getAll();
    $data['match']=$this->match_model->getAll();
    // print_r($data['match'][0]->team2_id);
    foreach($data['match'] as $key=>$match) {
      $data['team1'][]=$this->player_model->getAccToTeam($data['match'][0]->team1_id);
      // $data['team2'][]=$this->player_model->getAccToTeam($data['match'][1]->team2_id);
  }
    $this->load->view('base/header', $data);
    $this->load->view('eleven_form');
    $this->load->view('base/footer');
  }
}

/*    Function to validate the post of the form     */

function getAll() {
  $data['title'] = "View Players";        
  $data['all'] = $this->eleven_model->getAll();
  $this->load->view('base/header', $data);
  $this->load->view('eleven_table');
  $this->load->view('base/table_footer');
}

    /*
     * Function to update the destination 
     * id:destinaion identity  as argument 
     */

    function edit($id = '') {
//        $id = $this->uri->segment(3);
      if ($_POST) {
        $data = $this->eleven_model->getSingle($id);
        $this->match_model->manage($id);
        redirect('match/eleven');
      } else {
        $data['single'] = $this->match_model->getSingle($id);

        if (empty($data['single'])) {
          show_404();
        }
        $data['title'] = "Playing elevens";
        $this->load->view('base/header', $data);
        $this->load->view('eleven_form');
        $this->load->view('base/footer');
      }
    }

    /*
     * Function to Delete New Book 
     * id:Book identity  as argument 
     */

    function delete($id) {
      $this->eleven_model->delete($id);
      redirect('eleven');
    }

  }

  ?>
