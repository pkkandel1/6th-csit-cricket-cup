<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo base_url();?>dashboard"><i class="entypo-home"></i>Home</a>
    </li>
    <li>

        <a href="<?php echo base_url();?>match">Tables</a>
    </li>
    <li class="active">

        <strong>Match Table</strong>
    </li>
</ol>


<a href="<?php echo base_url();?>match/add" class="btn btn-success">
    <i class="entypo-plus"></i>
    Add New Match
</a>

<h2>Match Table</h2>

<br />
<?php if (!empty($message)) { ?>
<div class="alert alert-success"><b><?php echo $message; ?></b></div>
<?php } ?>
<?php print_r($all);?>
<table class="table table-bordered table-striped datatable" id="table-1">
    <thead>
        <tr>
            <th>S.N.</th>
            <th data-hide="team1_id">Team 1</th>
            <th data-hide="team2_id">Team 2</th>
            <th data-hide="match_day">Match Day</th>
            <th data-hide="match_type">Match Type</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($all as $a): ?>
            <tr class="odd gradeX">
                <td><?php echo $a->id;?></td>
                <td><?php echo $a->team1_id;?></td>
                <td><?php echo $a->team2_id; ?></td>
                <td><?php echo $a->match_day; ?></td>
                <td><?php echo $a->match_type; ?></td>

                <td>
                    <a href="<?php echo base_url();?>match/edit/<?php echo $a->id;?>" class="btn btn-default btn-sm btn-icon icon-left">
                        <i class="entypo-pencil"></i>
                        Edit
                    </a>

                    <a href="<?php echo base_url();?>match/delete/<?php echo $a->id;?>" class="btn btn-danger btn-sm btn-icon icon-left" onclick="javascript:return confirm('Are you sure?')">
                        <i class="entypo-cancel"></i>
                        Delete
                    </a>

                </td>
            </tr>
        <?php endforeach; ?>



    </tbody>

</table>

<script type="text/javascript">
    var responsiveHelper;
    var breakpointDefinition = {
        tablet: 1024,
        phone: 480
    };
    var tableContainer;

    jQuery(document).ready(function($)
    {
        tableContainer = $("#table-1");

        tableContainer.dataTable({
            "sPaginationType": "bootstrap",
            "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            "bStateSave": true,
            // Responsive Settings
            bAutoWidth: false,
            fnPreDrawCallback: function() {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper) {
                    responsiveHelper = new ResponsiveDatatablesHelper(tableContainer, breakpointDefinition);
                }
            },
            fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                responsiveHelper.createExpandIcon(nRow);
            },
            fnDrawCallback: function(oSettings) {
                responsiveHelper.respond();
            }
        });

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
    });
</script>

<br />



<script type="text/javascript">
    jQuery(window).load(function()
    {
        var $ = jQuery;

        $("#table-2").dataTable({
            "sPaginationType": "bootstrap",
            "sDom": "t<'row'<'col-xs-6 col-left'i><'col-xs-6 col-right'p>>",
            "bStateSave": false,
            "iDisplayLength": 8,
            "aoColumns": [
            {"bSortable": false},
            null,
            null,
            null,
            null
            ]
        });

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });

        // Highlighted rows
        $("#table-2 tbody input[type=checkbox]").each(function(i, el)
        {
            var $this = $(el),
            $p = $this.closest('tr');

            $(el).on('change', function()
            {
                var is_checked = $this.is(':checked');

                $p[is_checked ? 'addClass' : 'removeClass']('highlight');
            });
        });

        // Replace Checboxes
        $(".pagination a").click(function(ev)
        {
            replaceCheckboxes();
        });
    });

// Sample Function to add new row
var giCount = 1;

function fnClickAddRow()
{
    $('#table-2').dataTable().fnAddData(['<div class="checkbox checkbox-replace"><input type="checkbox" /></div>', giCount + ".2", giCount + ".3", giCount + ".4", giCount + ".5"]);

        replaceCheckboxes(); // because there is checkbox, replace it

        giCount++;
    }
</script>



<br />
<br />


<script type="text/javascript">
    jQuery(document).ready(function($)
    {
        var table = $("#table-3").dataTable({
            "sPaginationType": "bootstrap",
            "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            "bStateSave": true
        });

        table.columnFilter({
            "sPlaceHolder": "head:after"
        });
    });
</script>

<br />

<script type="text/javascript">
    jQuery(document).ready(function($)
    {
        var table = $("#table-4").dataTable({
            "sPaginationType": "bootstrap",
            "sDom": "<'row'<'col-xs-6 col-left'l><'col-xs-6 col-right'<'export-data'T>f>r>t<'row'<'col-xs-6 col-left'i><'col-xs-6 col-right'p>>",
            "oTableTools": {
            },
        });
    });

</script>

<br /><!-- Footer -->
