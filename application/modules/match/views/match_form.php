<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo base_url(); ?>dashboard"><i class="entypo-home"></i>Home</a>
    </li>
    <li>

        <a href="<?php echo base_url(); ?>match">Match</a>
    </li>
    <li class="active">

        <strong>Add</strong>
    </li>
</ol>
<a href="<?php echo base_url(); ?>match" class="btn btn-blue">
    <i class="entypo-search"></i>
    View Match
</a>
<h2>Add Match</h2>
<br />

<div class="panel panel-primary">

    <div class="panel-heading">
        <div class="panel-title">All fields have validation rules <small></small></div>

        <div class="panel-options">
            <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i class="entypo-cog"></i></a>
            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
            <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
            <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
        </div>
    </div>

    <div class="panel-body">

        <form role="form" id="form1" method="post" class="validate" action="" enctype="multipart/form-data">

            <div class="col-md-5">
             <div class="form-group">
                <label class="control-label">Team 1: </label>

                <select class="form-control" name="team_id"> 
                   <?php foreach($team as $t): ?>
                     <option value="<?php echo $t->id;?>" <?php if(!empty($single)){ if($single->team_id == $t->id){echo 'selected="selected"';}}?>> <?php echo $t->team_name;?> </option>
                 <?php endforeach;?>
             </select>
         </div>
     </div>  


     <div class="col-md-5">
         <div class="form-group">
            <label class="control-label">Team 2: </label>

            <select class="form-control" name="team_id"> 
               <?php foreach($team as $t): ?>
                 <option value="<?php echo $t->id;?>" <?php if(!empty($single)){ if($single->team_id == $t->id){echo 'selected="selected"';}}?>> <?php echo $t->team_name;?> </option>
             <?php endforeach;?>
         </select>
     </div>
 </div>


 <div class="col-md-11">
     <div class="form-group">
         <label class="control-label">Description</label>
         <textarea class="form-control ckeditor" name="description" data-validate="required">
            <?php if (!empty($single->description)) {
                echo $single->description;
            } else {
                echo set_value('description');
            }
            ?>
        </textarea>
    </div>
</div>



<div class="col-md-6">
    <label class="control-label">Match Date</label>

    <div class="input-group date" data-provide="datepicker">
        <input type="text" data-message-required="This field is required" class="form-control datepicker" value="<?php if(!empty($single)){ echo $single->match_day;}?>" name="match_day">
        <div class="input-group-addon">
            <span class="glyphicon glyphicon-th" ></span>
        </div>
    </div>
</div>

<div class="col-md-6">
    <div class="form-group">
        <label class="control-label">Umpire 1</label>

        <input type="text" class="form-control" name="ump1" data-validate="required" data-message-required="This field is required" placeholder="Player name here" value="<?php if(!empty($single)){ echo $single->ump1;}?>"/>
    </div>
</div> 

<div class="col-md-6">
    <div class="form-group">
        <label class="control-label">Umpire 2</label>

        <input type="text" class="form-control" name="ump2" data-validate="required" data-message-required="This field is required" placeholder="Player name here" value="<?php if(!empty($single)){ echo $single->ump2;}?>"/>
    </div>
</div> 


<div class="col-md-6">
    <div class="form-group">
        <label class="control-label">Man Of the Match</label>

        <input type="text" class="form-control" name="motm" data-validate="required" data-message-required="This field is required" placeholder="Player name here" value="<?php if(!empty($single)){ echo $single->motm;}?>"/>
    </div>
</div> 


<div class="col-md-6">
    <div class="form-group">
        <label class="control-label">Match Type</label>
        <select class="form-control" name="match_type"> 
            <option name="type" value="right">League </option>
            <option name="type" value="left">Play Offs </option>
        </select>
    </div>
</div>








<div class="col-md-6">
  <div class="form-group">
    <button type="submit" class="btn btn-success" name="submit">Submit</button>
    <button type="reset" class="btn">Reset</button>
</div>
</form>

</div>
</div>

</div><!-- Footer -->

<script type="text/javascript">
   $(function(){
    var date = $('.datepicker').datepicker({ dateFormat: 'dd-mm-yy' }).val();
});

</script>
