<?php

class Page extends MX_Controller {

    function __construct() {
        parent::__construct();
	$this->load->library('image_moo');
//        ob_start();
        $this->load->model('page/page_model');
        $this->load->model('category/category_model');
        if (!$this->input->is_ajax_request()) {
            modules::run('user/auth/check_login');
        }
    }

   public function index() {
        $this->getAll();
    }
 
   function do_upload() {
        $config['upload_path'] = "uploads/page_images/original";
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = '4000';
        $config['max_width'] = '2000';
        $config['max_height'] = '2000';
        $this->load->library('upload', $config);
        if ($this->upload->do_upload("userfile")) {
            $data = $this->upload->data();
            /* PATH */
            $source = "uploads/page_images/original/" . $data['file_name'];
            $destination_medium = "uploads/page_images/resized/";
            $destination_thumbs = "uploads/page_images/thumb/";
            $size_medium_width = 789;
            $size_medium_height = 524;
            $size_thumb_width = 354;
            $size_thumb_height = 228;
            $this->image_moo
                    ->load($source)
                    /* RESIZING IMAGE TO BE MEDIUM SIZE */
                    ->resize_crop($size_medium_width, $size_medium_height)
                    ->save($destination_medium . $data['file_name'])
                    /* CROPPING IMAGE TO BE THUMBNAIL SIZE */
                    ->resize_crop($size_thumb_width, $size_thumb_height)
                    ->save($destination_thumbs . $data['file_name']);
            if ($this->image_moo->errors)
                print $this->image_moo->display_errors();
            else {
                return $data['file_name'];
            }
        }
        else{
           $error = strip_tags($this->upload->display_errors());
          echo "<script type='text/javascript'>alert('.$error.');history.back(-1);</script>";
         die();          
        }
    }
   
     function add() {
       
        if ($_POST) {

	    if (!empty($_FILES['userfile']['name'])) {
            
            $img = $this->do_upload();
            
            
        } else {
            
            $img = '';
           
           
        }
             $this->page_model->manage($img);
            $this->getAll();

        } else {
            $data['title'] = "Add Page";
	    $data['parent'] = $this->page_model->getParent();
            $data['categories'] = $this->category_model->getAll();
		//print_r($data['parent']);die();
            $this->load->view('base/header',$data);
            $this->load->view('page_form');
            $this->load->view('base/footer');
        }
    }
    
   
    /*    Function to validate the post of the form     */

    function getAll() {

        $data['title'] = "View existing Pages";        
        $data['all'] = $this->page_model->getAll();
        $this->load->view('base/header', $data);
        $this->load->view('page/page_table');
        $this->load->view('base/table_footer');
    }

    /*
     * Function to update the destination 
     * id:destinaion identity  as argument 
     */

    function edit($id = '') {
//        $id = $this->uri->segment(3);
        if ($_POST) {
            $data = $this->page_model->getSingle($id);
	    $old = $data->image;
        if (!empty($_FILES['userfile']['name'])) {
            $img = $this->do_upload();
        } else {
            $img = $old;
        }
//print_r($img);die();
            $this->page_model->manage($img,$id);
            redirect('page');
        } else {
            $data['single'] = $this->page_model->getSingle($id);
            $data['categories'] = $this->category_model->getAll();
           
            if (empty($data['single'])) {
                show_404();
            }
            $data['title'] = "Edit Page";
	    $data['parent'] = $this->page_model->getParent();
            $this->load->view('base/header', $data);
            $this->load->view('page_form');
            $this->load->view('base/footer');
        }
    }

    /*
     * Function to Delete page
     * id:page identity  as argument 
     */

    function delete($id) {
        $this->page_model->delete($id);
        redirect('index.php/page');
    }

    function getPageData(){
        echo "This is just for test propose";
    }

}

?>