<ol class="breadcrumb bc-3">
    <li>
        <a href="index.html"><i class="entypo-home"></i>Home</a>
    </li>
    <li>

        <a href="<?php echo base_url(); ?>page">Page</a>
    </li>
    <li class="active">

        <strong>Add</strong>
    </li>
</ol>
<a href="<?php echo base_url(); ?>page" class="btn btn-blue">
    <i class="entypo-search"></i>
    View Page
</a>
<h2>Add Page</h2>
<br />

<div class="panel panel-primary">

    <div class="panel-heading">
        <div class="panel-title">All fields have validation rules <small></small></div>

        <div class="panel-options">
            <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i class="entypo-cog"></i></a>
            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
            <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
            <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
        </div>
    </div>

    <div class="panel-body">

        <form role="form" id="form1" method="post" class="validate" action="" enctype="multipart/form-data">

            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Page Name</label>

                    <input type="text" class="form-control" name="name" data-validate="required" data-message-required="This field is required"
                     placeholder="Page name here" value="<?php if (!empty($single)) {
    echo $single->name;
} ?>"/>
                </div>
            </div> 

            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Parent Of : </label>

                    <select class="form-control" name="parent_id"> 
                        <option value="0">none</option>
                        <?php foreach ($parent as $p): ?>

                            <option value="<?php echo $p->id; ?>" <?php if (!empty($single)) {
                                if ($p->id == $single->parent_id) {
                                    echo 'selected="selected"';
                                }
                            } ?>><?php echo $p->name; ?></option>
<?php endforeach; ?>
                    </select>
                </div>
            </div>	    
<div class="clearfix"></div>
                      
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Category : </label>

                    <select class="form-control" name="category" id="category"> 
                        <option value="0">none</option>
                        <?php foreach ($categories as $c): ?>

                            <option value="<?php echo $c->id; ?>" <?php if (!empty($single)) {
                                if ($c->id == $single->cat_id) {
                                    echo 'selected="selected"';
                                }
                            } ?>><?php echo $c->cat_name; ?></option>
<?php endforeach; ?>
                    </select>
                </div>
            </div>
            
              <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Images</label><br>

                    <input type="file" class="form-control file2 inline btn btn-primary" multiple="1" data-label="<i class='glyphicon glyphicon-circle-arrow-up' ></i> &nbsp;Browse Files" name="userfile" />
                </div>
            </div>

         <div class="col-md-6" id="testData">
                <div class="form-group">
                    <label class="control-label">Date</label><br>
                    <input type="text" name="date" class="form-group" id="dateexample" value="<?php if (!empty($single)) {
                echo $single->date;
        } ?>">
                 </div>
        </div>

            <div class="col-md-11">
                <div class="form-group">
                    <label class="control-label">Description</label>
    <textarea id="ckeditor" class="ckeditor" name="description" data-validate="required" >
<?php
if (!empty($single->description)) {
    echo $single->description;
} else {
    echo set_value('description');
}
?>
                    </textarea>

                     <script type="text/javascript">
            //<![CDATA[

                // This call can be placed at any point after the
                // <textarea>, or inside a <head><script> in a
                // window.onload event handler.

                // Replace the <textarea id="editor"> with an CKEditor
                // instance, using default configurations.
                CKEDITOR.replace( 'ckeditor',
                {
                    filebrowserBrowseUrl :'<?php echo base_url();?>assets/js/ckeditor/filemanager/browser/default/browser.html?Connector=<?php echo base_url();?>assets/js/ckeditor/filemanager/connectors/php/connector.php',
                    filebrowserImageBrowseUrl : '<?php echo base_url();?>assets/js/ckeditor/filemanager/browser/default/browser.html?Type=Image&Connector=<?php echo base_url();?>assets/js/ckeditor/filemanager/connectors/php/connector.php',
                    filebrowserFlashBrowseUrl :'<?php echo base_url();?>assets/js/ckeditor/filemanager/browser/default/browser.html?Type=Flash&Connector=<?php echo base_url();?>assets/js/ckeditor/filemanager/connectors/php/connector.php',
                    filebrowserUploadUrl  :'<?php echo base_url();?>assets/js/ckeditor/filemanager/connectors/php/upload.php?Type=File',
                    filebrowserImageUploadUrl : '<?php echo base_url();?>assets/js/ckeditor/filemanager/connectors/php/upload.php?Type=Image',
                    filebrowserFlashUploadUrl : '<?php echo base_url();?>assets/js/ckeditor/filemanager/connectors/php/upload.php?Type=Flash'
                });

            //]]>
            </script>

            
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Meta Title</label>
                    <textarea class="form-control autogrow" name="meta_title" data-validate="minlength[10]" rows="5" placeholder="Meta Title here" ><?php if (!empty($single)) {
    echo $single->meta_title;
} ?></textarea>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Meta Keyword</label>
                    <textarea class="form-control autogrow" name="meta_keyword" data-validate="minlength[10]" rows="5" placeholder="Meta Title here"><?php if (!empty($single)) {
    echo $single->meta_keyword;
} ?></textarea>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Meta Description</label>
                    <textarea class="form-control autogrow" name="meta_description" data-validate="minlength[10]" rows="5" placeholder="Meta Description here"><?php if (!empty($single)) {
    echo $single->meta_description;
} ?></textarea>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Publish</label>
                    <ul class="icheck-list">
                        <li>
                            <input tabindex="5" type="checkbox" class="icheck" name="publish" value="1" id="minimal-checkbox-1" <?php if (!empty($single)) {
    if ($single->publish == "1") {
        echo "checked";
    }
} ?>>
                            <label for="minimal-checkbox-1"></label>
                        </li>

                    </ul>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Display</label>
                    <ul class="icheck-list">
                        <li>
                            <input tabindex="5" type="checkbox" class="icheck" name="featured" value="1" id="minimal-checkbox-1" <?php if (!empty($single)) {
    if ($single->featured == "1") {
        echo "checked";
    }
} ?>>
                            <label for="minimal-checkbox-1"></label>
                        </li>

                    </ul>
                </div>
            </div>

            <div class="col-md-11">
                <div class="form-group">
                    <button type="submit" class="btn btn-success" name="submit">Submit</button>
                    <button type="reset" class="btn">Reset</button>
                </div>
        </form>

    </div>
</div>

</div><!-- Footer -->
<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>



<script type="text/javascript">


    $( "#testData" ).hide();
   $('select').change(function(){
       

    var getSelect = $('#category').val();
    if(getSelect == 6 ){
         $( "#testData" ).show();
    
    }else{
            $( "#testData" ).hide();
    }
   });

    $('#dateexample').datepicker({
                    format: "yyyy-mm-dd"
                });

</script>