<?php

class Page_model extends CI_Model {
    
    private $table = "mk_page";

    function construct() {
        parent::__construct();
    }

    function index() {

        $this->load->view('admin/post');
    }

    /*
     * Function to add destination from form
     */

    function manage($img='',$id='') {	
	$slug = url_title($this->input->post('name'), 'dash', true);   
      
        $data = array(
	    'parent_id' => $this->input->post('parent_id'),
            'cat_id' => $this->input->post('category'),
            'name' => $this->input->post('name'),            
	    'image'=>$img,
	    'slug' => $slug,
            'description' => $this->input->post('description'),
            'meta_title' => $this->input->post('meta_title'),
            'meta_keyword' => $this->input->post('meta_keyword'),    
	    'meta_description' => $this->input->post('meta_description'),       
	    'publish' => $this->input->post('publish'),
	    'featured' => $this->input->post('featured'),  
        'date' => $this->input->post('date'), 
        );

      





        if ($id == '') {
		
            $this->db->insert($this->table, $data);
            $this->session->set_flashdata('message', "Insertion successfull");
        } else {           
            $this->db->where('id', $id);
            $this->db->update($this->table, $data);
            $this->session->set_flashdata('message',"Information updated successfully");
        }
    }

   

    function getAll() {
        $this->db->select('*');
        $this->db->from($this->table);
        $query = $this->db->get();
        return $query->result();
    }

  
    function getSingle($id) {
        $data = $this->db->query("SELECT * FROM $this->table WHERE id='$id'");
        return $data->row($id);
    }    
    
    function delete($id){
        $this->db->delete($this->table,array('id'=>$id));
        $this->session->set_flashdata('message',"Information Deleted successfully");        
    }

    function getParent(){
	$data = $this->db->query("SELECT * FROM $this->table WHERE parent_id='0'");
	return $data->result();
	}

/* front end functions start here */
	function getSingleFront($slug){
	$id = $this->getIdbySlug($slug);
	$data = $this->db->query("SELECT * FROM $this->table WHERE id='$id' AND publish='1'");
	return $data->row($slug);
	}
        
        function getSinglePageBySlug($slug){
            
            $data = $this->db->query("Select * from $this->table where slug='$slug' AND publish='1'") ;
            return $data->row($slug);
            
            
        }

	function getIdbySlug($slug){
	   $query = $this->db->query("Select id from $this->table where slug='$slug'");
	   $data = $query->row($slug);
	   if(!empty($data)){
	   return $data->id;
	}
	}

	function getPublished(){
	$data = $this->db->query("SELECT * FROM $this->table WHERE featured='1' LIMIT 3");
	return $data->result();
	}
	
	 function getSubpage($pid) { 
        $result = array();
        $sql = "SELECT * FROM mk_page where parent_id = '$pid'";
        $query = $this->db->query($sql);
        $result[] = $query->result();
        return $result[0];
    }
	
	function getAllonHome() {
        $query = $this->db->query("select * from mk_page where publish=1 and featured=1");
        return $query->result();
    }
	
	function getsubpagesByslug($slug){
	
	$id = $this->getIdbySlug($slug);
	$data = $this->db->query("SELECT * FROM $this->table WHERE parent_id='$id' AND publish='1'");
	return $data->result();
	}
        
        function getCatIdbySlug($slug){
	   $query = $this->db->query("Select id from mk_category where slug='$slug'");
	   $data = $query->row($slug);
	   if(!empty($data)){
	   return $data->id;
	}
	}
        
        function getByCategory($slug){
	$cid = $this->getCatIdbySlug($slug);
	$data = $this->db->query("SELECT * FROM $this->table WHERE cat_id='$cid' AND publish='1'");
	return $data->result();
	}
        
        function getSingleByCategory($slug){
	$id = $this->getIdbySlug($slug);
	$data = $this->db->query("SELECT * FROM $this->table WHERE id='$id' AND publish='1'");
	return $data->row($slug);
	}
        
        function getFeaturedByCategory($slug){
	$cid = $this->getCatIdbySlug($slug);
	$data = $this->db->query("SELECT * FROM $this->table WHERE cat_id='$cid' AND publish='1' AND featured='1'");
	return $data->result();
	}
	
        function getTypeBySlug($slug){
	   $query = $this->db->query("Select cat_id from $this->table where slug='$slug'");
	   $data = $query->row($slug);
	   if(!empty($data)){
	   return $data->cat_id;
	}
       
	}
        function getAllKeyFeatures(){
            
            $query=$this->db->query("Select * from $this->table where cat_id='5'");
            return $query->result();
            
        }
   function getAllNewsEvents(){
            
            $query=$this->db->query("Select * from $this->table where cat_id='6' and publish='1' ORDER BY date DESC limit 3");
            return $query->result();
            
        
        $data['title'] = 'News and Events of Kamyak school';
        $data['images'] = $this->image_model->getAll();
        $data['news'] = $this->page_model->getAllNewsEvents();
        $this->load->view('home/news', $data);
    }
}

?>
