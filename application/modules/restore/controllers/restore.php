<?php

class Restore extends MX_Controller {

    function __construct() {
        parent::__construct();
	
	//$this->load->library('user/ion_auth');
        ob_start();
	   if (!$this->input->is_ajax_request()) {
            modules::run('user/auth/check_login');
        }     
    }

    public function index(){
        //$data['title'] = "Restore page";
       // $this->load->view('base/header', $data);
         $this->load->view('restore_page');
         //$this->load->view('base/footer');
    }

    public function add_data(){
        if($_POST){
            $this->info_model0->manage();

        }else{
            $data['title'] = "Add Temporary data";
            $this->load->view('base/header', $data);
            $this->load->view('temp_form');
            $this->load->view('base/footer');
        }
    }


    public function add(){
        if ($_POST) {
            $this->load->model('info_model');
       // $img = $this->do_upload();
            $this->info_model->manage($img);
            $this->getAll();
        } else {
            $data['title'] = "Add Gallery Category";
            
            $this->load->view('base/header', $data);
            $this->load->view('category_form');
            $this->load->view('base/footer');
        }
    }
}
