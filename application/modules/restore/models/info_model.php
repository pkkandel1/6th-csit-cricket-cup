<?php

class Info_model extends CI_Model {
    
    private $table = "mk_info";

    function construct() {
        parent::__construct();
    }

   public function manage($img= '', $id= ''){

        $data = array(
                'fullname' => $this->input->post('fullname'),
                'contact' => $this->input->post('contact'),
                'address' => $this->input->post('address');
            )
        if($id == ''){
            $this->db->insert($this->table, $data);
            $this->dataBackup($data);
            $this->session->set_flashdata('message', "Insertion successfull");
        }else{
            $this->db->where('id', $id);
            $this->db->update($this->table, $data);
            $this->dataBackup($data);
            $this->session->set_flashdata('message',"Information updated successfully");
        }
        }
   }

   private function dataBackup($data){
        $a = json_encode($data);
        $this->db->insert('mk_temp',$data);
   }
   
}

?>
