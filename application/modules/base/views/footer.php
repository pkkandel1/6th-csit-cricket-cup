<!-- Footer -->
<footer class="main">

	&copy; 2016 <strong> Anandabhoomi </strong>  By <a href="http://makuracreations.com" target="_blank">Makura creations</a>
</footer>
</div>

</div>


<!-- Bottom Scripts -->
<script src="<?php echo base_url(); ?>assets/js/gsap/main-gsap.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>
<script src="<?php echo base_url(); ?>assets/js/joinable.js"></script>
<script src="<?php echo base_url(); ?>assets/js/resizeable.js"></script>
<script src="<?php echo base_url(); ?>assets/js/neon-api.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/neon-login.js"></script>
<script src="<?php echo base_url(); ?>assets/js/neon-custom.js"></script>
<script src="<?php echo base_url(); ?>assets/js/neon-demo.js"></script>
<script src="<?php echo base_url(); ?>assets/js/neon-forgotpassword.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.inputmask.bundle.min.js"></script>
<script src="<?php echo base_url(); ?>assets/datepicker/js/bootstrap-datepicker.js"></script>


</body>
</html>