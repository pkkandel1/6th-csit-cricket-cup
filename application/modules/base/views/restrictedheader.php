
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="Anandabhoomi Admin Panel" />
	<meta name="author" content="" />
	
        <title><?php if(empty($title)){$title="Anandabhoomi";}echo  $title;?></title>
	

	<!--<link rel="stylesheet" href="<?php echo base_url();?>assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">-->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/font-icons/entypo/css/entypo.css">
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.css" media="all">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/neon-core.css" media="all">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/neon-theme.css" media="all">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/neon-forms.css" media="all">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/custom.css" media="all">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/font-icons/font-awesome/css/font-awesome.min.css">
	<script src="<?php echo base_url();?>assets/js/jquery-1.11.0.min.js"></script>

	<!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
	
	
</head>
<body class="page-body  page-fade">

<div class="page-container">
<div class="sidebar-menu">
	<header class="logo-env">
		<!-- logo -->
		<div class="logo">
		<a href="<?php echo base_url();?>">
					<img src="<?php echo base_url();?>assets/images/smalllogo.png">
			</a>
		</div>

		<!-- logo collapse icon -->

		<div class="sidebar-collapse">
			<a href="#" class="sidebar-collapse-icon with-animation">
				<!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
				<i class="entypo-menu"></i>
			</a>
		</div>


		<!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
		<div class="sidebar-mobile-menu visible-xs">
			<a href="#" class="with-animation"><!-- add class "with-animation" to support animation -->
				<i class="entypo-menu"></i>
			</a>
		</div>

	</header>


	<ul id="main-menu" class="">
		<!-- add class "multiple-expanded" to allow multiple submenus to open -->
		<!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->
		<!-- Search Bar -->
<!--		<li id="search">
			<form method="get" action="">
				<input type="text" name="q" class="search-input" placeholder="Search something..."/>
				<button type="submit">
					<i class="entypo-search"></i>
				</button>
			</form>
		</li>-->
		<li class="active opened active">
			<a href="<?php echo base_url();?>">
				<i class="entypo-gauge"></i>
				<span>Dashboard</span>
			</a>
		</li>
		<li>
			<a href="<?php echo base_url();?>agent">
				<i class="entypo-ticket"></i>
				<span>Reservations</span>
			</a>
			<ul>
				<li>
					<a href="<?php echo base_url();?>reservation/add">
						<i class="entypo-plus"></i>
						<span>Add New</span>
					</a>
				</li>
				<li>
					<a href="<?php echo base_url();?>reservation">
						<i class="entypo-ticket"></i>
						<span>View Reservations</span>
					</a>
				</li>

			</ul>
		</li>


		<li>
			<a href="<?php echo base_url();?>agent">
				<i class="entypo-users"></i>
				<span>Agents</span>
			</a>
			<ul>
				<li>
					<a href="<?php echo base_url();?>agent/add">
						<i class="entypo-plus"></i>
						<span>Add Agent</span>
					</a>
				</li>
				<li>
					<a href="<?php echo base_url();?>agent">
						<i class="entypo-ticket"></i>
						<span>Agent Reservations</span>
					</a>
				</li>

			</ul>
		</li>
<?php if(!empty($role) && $role =="admin"){?>
		<li>
			<a href="#">
				<i class="entypo-doc-text"></i>
				<span>Programs</span>
			</a>
			<ul>
				<li>
					<a href="<?php echo base_url();?>room/category">
						<i class="fa fa-file-text-o fa-fw"></i>
						<span>Room Category</span>
					</a>
				</li>
                                
                                <li>
					<a href="<?php echo base_url();?>room">
						<i class="entypo-home"></i>
						<span>Tents</span>
					</a>
				</li>
                                
                                <li>
					<a href="<?php echo base_url();?>room/availability">
						<i class="fa fa-check-square fa-fw"></i>
						<span>Room Availability</span>
					</a>
				</li>
                                
				<li>
					<a href="<?php echo base_url();?>activity">
						<i class="entypo-layout"></i>
						<span>Activity</span>
					</a>
				</li>
			</ul>
		</li>
                
                <li>
			<a href="#">
				<i class="entypo-user"></i>
				<span>User</span>
			</a>
			<ul>
				<li>
					<a href="<?php echo base_url();?>user/add">
						<i class="entypo-plus"></i>
						<span>Add User</span>
					</a>
				</li>
				<li>
					<a href="<?php echo base_url();?>user/all">
						<i class="fa fa-eye fa-fw"></i>
						<span>View Users</span>
					</a>
				</li>

			</ul>
		</li>
<?php }?>
	</ul>

</div>
<div class="main-content">

<div class="row">

	<!-- Profile Info and Notifications -->
	<div class="col-md-6 col-sm-8 clearfix">

		<ul class="user-info pull-left pull-right-xs pull-none-xsm">


                            <!-- Message Notifications -->
                            <?php $url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";  
                            if($url==base_url()){?>
                            <li class="notifications dropdown">

                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                                   data-close-others="true">
                                    <i class="entypo-bell"></i>
                                    <span class="badge badge-warning"><?php echo count($notification);?></span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li>
                                        <ul class="dropdown-menu-list scroller">
                                            
                                        <?php foreach ($notification as $n): ?>
                                                <li class="active">
                                                    <a href="#">
<!--                                                        <span class="image pull-right">
                                                            <img src="assets/images/thumb-1.png" alt="" class="img-circle"/>
                                                        </span>-->

                                                        <span class="line">
                                                            <strong><?php echo $n->full_name;?></strong>
                                                            - <?php echo $n->aname;?>
                                                        </span>

                                                        <span class="line desc small">
                                                           <?php echo $n->remarks;?>
                                                        </span>
                                                    </a>
                                                </li>
                                            <?php endforeach; ?>


                                        </ul>
                                    </li>

<!--                                    <li class="external">
                                        <a href="#">All Notification</a>
                                    </li>-->
                                </ul>

                            </li>
                            <?php } ?>
                        </ul>


	</div>
	<div class="col-md-6 col-sm-4 clearfix hidden-xs">

<!--		<ul class="list-inline links-list pull-right">-->
<!---->
<!---->
<!--			<li>-->
<!--				<a href="--><?php //echo base_url();?><!--user/logout">-->
<!--					Log Out <i class="entypo-logout right"></i>-->
<!--				</a>-->
<!--			</li>-->
<!--		</ul>-->


		<ul class="user-info pull-right pull-none-xsm">

			<!-- Profile Info -->
			<li class="profile-info dropdown"><!-- add class "pull-right" if you want to place this from right -->

				<a href="#" class="dropdown-toggle" data-toggle="dropdown">
					<img src="<?php echo base_url();?>uploads/user_images/thumb/<?php echo $this->session->userdata('userimage');?>" alt="" class="img-thumb"
					     width="44"/>
					<?php echo $this->session->userdata('username');?>
				</a>

				<ul class="dropdown-menu">

					<!-- Reverse Caret -->
					<li class="caret"></li>

					<!-- Profile sub-links -->
					<li>
						<a href="<?php echo base_url();?>user/profile">
							<i class="entypo-user"></i>
							View Profile
						</a>
					</li>
					<li>
						<a href="<?php echo base_url();?>user/logout">
							Log Out <i class="entypo-logout right"></i>
						</a>
					</li>
				</ul>
			</li>

		</ul>
	</div>

</div>

<hr/>

