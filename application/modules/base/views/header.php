<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="description" content="Csit Cup Admin Panel" />
        <meta name="author" content="" />

        <title><?php
            if (empty($title)) {
                $title = "CSIT Cup";
            }echo $title;
            ?></title>


        <!--<link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-icons/entypo/css/entypo.css">
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css" media="all">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/neon-core.css" media="all">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/neon-theme.css" media="all">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/neon-forms.css" media="all">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/custom.css" media="all">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-icons/font-awesome/css/font-awesome.min.css">
        <script src="<?php echo base_url(); ?>assets/js/jquery-1.11.0.min.js"></script>
            <script src="<?php echo base_url(); ?>assets/js/ckeditor/ckeditor.js"></script>

        <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
                <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
                <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->


    </head>
    <body class="page-body  page-fade">

        <div class="page-container">
            <div class="sidebar-menu">
                <header class="logo-env">
                    <!-- logo -->
                    <div class="logo">
                        <a href="<?php echo base_url(); ?>dashboard">
                            <img src="<?php echo base_url(); ?>assets/home/images/logop.png" width="220px;">
                        </a>
                    </div>

                    <!-- logo collapse icon -->

                    <div class="sidebar-collapse">
                        <a href="#" class="sidebar-collapse-icon with-animation">
                            <!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
                            <i class="entypo-menu"></i>
                        </a>
                    </div>


                    <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
                    <div class="sidebar-mobile-menu visible-xs">
                        <a href="#" class="with-animation"><!-- add class "with-animation" to support animation -->
                            <i class="entypo-menu"></i>
                        </a>
                    </div>

                </header>


                <ul id="main-menu" class="">
                    <!-- add class "multiple-expanded" to allow multiple submenus to open -->
                    <!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->
                    <!-- Search Bar -->
                    <!--		<li id="search">
                                            <form method="get" action="">
                                                    <input type="text" name="q" class="search-input" placeholder="Search something..."/>
                                                    <button type="submit">
                                                            <i class="entypo-search"></i>
                                                    </button>
                                            </form>
                                    </li>-->
                    <li class="active opened active">
                        <a href="<?php echo base_url(); ?>dashboard">
                            <i class="entypo-gauge"></i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    


                    <li>
                        <a href="<?php echo base_url(); ?>team">
                            <i class="entypo-doc-text"></i>
                            <span>Teams</span>
                        </a>
                        <ul>
                            <li>
                                <a href="<?php echo base_url(); ?>team/add">
                                    <i class="entypo-plus"></i>
                                    <span>Add New Team</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>team">
                                    <i class="entypo-ticket"></i>
                                    <span>View Teams</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>team/player/add">
                                    <i class="entypo-plus"></i>
                                    <span>Add Players</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>team/player">
                                    <i class="entypo-ticket"></i>
                                    <span>View Players</span>
                                </a>
                            </li>
                          


                        </ul>
                    </li>


                    <li>
                        <a href="<?php echo base_url(); ?>match">
                            <i class="entypo-ticket"></i>
                            <span>Match</span>
                        </a>
                        <ul>
                            <li>
                                <a href="<?php echo base_url(); ?>match/add">
                                    <i class="entypo-plus"></i>
                                    <span>Add New Match</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>match">
                                    <i class="entypo-ticket"></i>
                                    <span>View All Matches</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>match/eleven">
                                    <i class="entypo-ticket"></i>
                                    <span>View Playing Eleven</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>match/eleven/add">
                                    <i class="entypo-plus"></i>
                                    <span>Add Playing Eleven</span>
                                </a>
                            </li>

                        </ul>
                    </li>


                    <li>
                        <a href="<?php echo base_url(); ?>article">
                            <i class="entypo-ticket"></i>
                            <span>Update</span>
                        </a>
                        <ul>
                            <li>
                                <a href="<?php echo base_url(); ?>article/add">
                                    <i class="entypo-plus"></i>
                                    <span>Add New Update</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>article">
                                    <i class="entypo-ticket"></i>
                                    <span>View Updates</span>
                                </a>
                            </li>

                        </ul>
                    </li>

                    

                    
                    <?php
                           $role= $this->session->userdata('role');
                           if($role == 'admin'):
                    ?>
                
                    <li>
                        <a href="#">
                            <i class="entypo-user"></i>
                            <span>User</span>
                        </a>
                        <ul>
                            <li>
                                <a href="<?php echo base_url(); ?>user/add">
                                    <i class="entypo-plus"></i>
                                    <span>Add User</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>user/all">
                                    <i class="fa fa-eye fa-fw"></i>
                                    <span>View Users</span>
                                </a>
                            </li>

                        </ul>
                    </li>

                        <?php endif; ?>

                    
                   

                  
                </ul>

            </div>
            <div class="main-content">

                <div class="row">

                    <!-- Profile Info and Notifications -->
                    <div class="col-md-3 col-sm-4 clearfix">

                        <ul class="user-info pull-left pull-right-xs pull-none-xsm">


                            <!-- Message Notifications -->
                            <?php
                            $url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

                            if ($url == base_url() . 'dashboard') {
                                ?>
                                <li class="notifications dropdown">

                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                                       data-close-others="true">
                                        <i class="entypo-bell"></i>
                                       
                                    </a>

                                    <ul class="dropdown-menu">


                                        <li>
                                            <ul class="dropdown-menu-list scroller">

                                                <?php foreach ($notification as $n): ?>
                                                    <li class="active">
                                                        <a href="#">
                                                            <span class="image pull-right">
                                                                <img src="<?php echo base_url() ?>uploads/user_images/thumb/<?php echo $n->image; ?>" alt="" class="img-circle"/>
                                                            </span>

                                                            <span class="line">
                                                                <strong><?php echo $n->username; ?></strong>
                                                                Performed <?php echo $n->operation; ?> Operation on the reservation of <?php echo $n->full_name; ?>
                                                            </span>

                                                            <span class="line desc small">
                                                                @  <?php echo $n->time; ?>
                                                            </span>
                                                        </a>
                                                    </li>
                                                <?php endforeach; ?>


                                            </ul>
                                        </li>

                                        <!--                                                                            <li class="external">
                                                                                                                            <a href="#">All Notification</a>
                                                                                                                        </li>-->
                                    </ul>

                                </li>
                            <?php } ?>
                        </ul>


                    </div>
                    <div class="col-md-5 col-sm-4">
                        <div class="clock">
                            <div class="dateWrap"><i class="entypo-calendar" style="float: left; width: 9%;font-size: 17px;margin-top: 0.4em;color: #008D4C;"></i> <div id="Date"></div>
                                <div class="clearfix"></div>
                            </div>

                            <ul>
                                <li><i class="entypo-clock"></i> </li>
                                <li id="hours"> </li>
                                <li id="point">:</li>
                                <li id="min"> </li>
                                <li id="point">:</li>
                                <li id="sec"> </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 clearfix hidden-xs">

                        <!--		<ul class="list-inline links-list pull-right">-->
                        <!---->
                        <!---->
                        <!--			<li>-->
                        <!--				<a href="--><?php //echo base_url();   ?><!--user/logout">-->
                        <!--					Log Out <i class="entypo-logout right"></i>-->
                        <!--				</a>-->
                        <!--			</li>-->
                        <!--		</ul>-->


                        <ul class="user-info pull-right pull-none-xsm">

                            <!-- Profile Info -->
                            <li class="profile-info dropdown"><!-- add class "pull-right" if you want to place this from right -->

                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="<?php echo base_url(); ?>uploads/user_images/thumb/<?php echo $this->session->userdata('userimage'); ?>" alt="" class="img-thumb"
                                         width="44"/>
                                         <?php echo $this->session->userdata('username'); ?>
                                </a>

                                <ul class="dropdown-menu">

                                    <!-- Reverse Caret -->
                                    <li class="caret"></li>

                                    <!-- Profile sub-links -->
                                    <li>
                                        <a href="<?php echo base_url(); ?>user/profile">
                                            <i class="entypo-user"></i>
                                            View Profile
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url(); ?>user/logout">
                                            Log Out <i class="entypo-logout right"></i>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                        </ul>
                    </div>

                </div>

                <hr/>

                <script type="text/javascript">
                    $(document).ready(function() {
                        // Create two variable with the names of the months and days in an array
                        var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                        var dayNames = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]

                        // Create a newDate() object
                        var newDate = new Date();
                        // Extract the current date from Date object
                        newDate.setDate(newDate.getDate());
                        // Output the day, date, month and year
                        $('#Date').html(dayNames[newDate.getDay()] + " " + newDate.getDate() + ' ' + monthNames[newDate.getMonth()] + ' ' + newDate.getFullYear());

                        setInterval(function() {
                            // Create a newDate() object and extract the seconds of the current time on the visitor's
                            var seconds = new Date().getSeconds();
                            // Add a leading zero to seconds value
                            $("#sec").html((seconds < 10 ? "0" : "") + seconds);
                        }, 1000);

                        setInterval(function() {
                            // Create a newDate() object and extract the minutes of the current time on the visitor's
                            var minutes = new Date().getMinutes();
                            // Add a leading zero to the minutes value
                            $("#min").html((minutes < 10 ? "0" : "") + minutes);
                        }, 1000);

                        setInterval(function() {
                            // Create a newDate() object and extract the hours of the current time on the visitor's
                            var hours = new Date().getHours();
                            // Add a leading zero to the hours value
                            $("#hours").html((hours < 10 ? "0" : "") + hours);
                        }, 1000);

                    });
                </script>
