<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo base_url(); ?>dashboard"><i class="entypo-home"></i>Home</a>
    </li>
    <li>

        <a href="<?php echo base_url(); ?>article">Article</a>
    </li>
    <li class="active">

        <strong>Add</strong>
    </li>
</ol>
<a href="<?php echo base_url(); ?>article" class="btn btn-blue">
    <i class="entypo-search"></i>
    View Articles
</a>
<h2>Add Articles</h2>
<br />

<div class="panel panel-primary">

    <div class="panel-heading">
        <div class="panel-title">All fields have validation rules <small></small></div>

        <div class="panel-options">
            <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i class="entypo-cog"></i></a>
            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
            <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
            <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
        </div>
    </div>

    <div class="panel-body">

        <form role="form" id="form1" method="post" class="validate" action="" enctype="multipart/form-data">

            <div class="col-md-11">
                <div class="form-group">
                <label class="control-label">News Title</label>

                    <input type="text" class="form-control" name="title" data-validate="required" data-message-required="This field is required" placeholder="Title Here" value="<?php if(!empty($single)){ echo $single->title;}?>"/>
                </div>
            </div>

            <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">Featured Image</label><br>

                <input type="file" name="featured_image" class="form-control inline btn btn-primary" multiple=""><font color="red" data-label="<i class='glyphicon glyphicon-circle-arrow-up'></i> &nbsp;Browse Files"></font>

            </div>
        </div>

            <div class="col-md-11">
               <div class="form-group">
                   <label class="control-label">Description</label>
                   <textarea class="form-control ckeditor" name="description" data-validate="required">
                    <?php if (!empty($single->description)) {
                        echo $single->description;
                    } else {
                        echo set_value('description');
                    }
                    ?>
                </textarea>
            </div>
        </div>

        

        <div class="col-md-6">
                <label class="control-label">Published Date</label>

                <div class="input-group date" data-provide="datepicker">
                    <input type="text" data-message-required="This field is required" class="form-control datepicker" value="<?php if(!empty($single)){ echo $single->published_date;}?>" name="published_date">
                    <div class="input-group-addon">
                        <span class="glyphicon glyphicon-th" ></span>
                    </div>
                </div>
            </div>

        

        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">Publish</label>
                <ul class="icheck-list">
                    <li>
                        <input tabindex="5" type="checkbox" class="icheck" name="publish" value="1" id="minimal-checkbox-1" <?php if(!empty($single)){ if($single->publish=="1"){echo "checked";}}?>>
                        <label for="minimal-checkbox-1"></label>
                    </li>

                </ul>
            </div>
        </div>

        <div class="col-md-5">
            <div class="form-group">
                <label class="control-label">Featured</label>
                <ul class="icheck-list">
                    <li>
                        <input tabindex="5" type="checkbox" class="icheck" name="featured" value="1" id="minimal-checkbox-1" <?php if(!empty($single)){ if($single->featured=="1"){echo "checked";}}?>>
                        <label for="minimal-checkbox-1"></label>
                    </li>

                </ul>
            </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <button type="submit" class="btn btn-success" name="submit">Submit</button>
            <button type="reset" class="btn">Reset</button>
        </div>
    </form>

</div>
</div>

</div><!-- Footer -->

<script type="text/javascript">
 $(function(){
        var date = $('.datepicker').datepicker({ dateFormat: 'dd-mm-yy' }).val();
    });

</script>
