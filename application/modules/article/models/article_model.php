<?php

class Article_model extends CI_Model {

    private $table = "tbl_news_update";

    function construct() {
        parent::__construct();
    }

    function index() {

        $this->load->view('admin/post');
    }

    /*
     * Function to add destination from form
     */

    function manage($img = '', $id='') {

        $data = array(
           'title' => $this->input->post('title'),
           'description' => $this->input->post('description'),
           'published_date' => $this->input->post('published_date'),  
           'featured_image' => $img,    
           'publish' => $this->input->post('publish'), 
           'featured' => $this->input->post('featured') 
           );

        if ($id == '') {

            $this->db->insert($this->table, $data);
            $this->session->set_flashdata('message', "Insertion successfull");
        } else {           
            $this->db->where('id', $id);
            $this->db->update($this->table, $data);
            $this->session->set_flashdata('message',"Information updated successfully");
        }
    }



    function getAll() {
        $this->db->select('*');
        $this->db->from($this->table);
        $query = $this->db->get();
        return $query->result();
    }


    function getSingle($id) {
        $data = $this->db->query("SELECT * FROM $this->table WHERE id='$id'");
        return $data->row($id);
    }
    
    function delete($id){
        $this->db->delete($this->table,array('id'=>$id));
        $this->session->set_flashdata('message',"Information Deleted successfully");        
    }

    function getPublish(){
	$data=$this->db->query("SELECT * FROM $this->table WHERE publish='1' ORDER BY published_date DESC limit 4");
	return $data->result();
    }

   

    function getOtherArticle() {
        $this->db->select('*');
    $this->db->from($this->table);
    $this->db->where('publish',1);
    $this->db->where('type','3');
    $this->db->limit(2);
    $query = $this->db->get();
    return $query->result();
    }

}

?>
