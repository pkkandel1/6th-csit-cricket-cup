<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

Class MY_Model extends CI_model {
	function __Construct() {
		parent:: __construct();
	}

	protected function insert($detail){
		$table = $detail['table'];
		$input = $detail['input'];
		$this->db->insert($table, $input);
		$this->session->set_flashdata('alert', "success");
		$this->session->set_flashdata('msg', "Congratulations !! Your data has been inserted successfully !!");
	}


	//abstract protected function test();

	public function hello(){
		echo "Hello";die();
	}

	public function update($detail) {
		$id = $detail['id'];
		$data = $detail['input'];
		$table = $detail['table'];
		$this->db->where('id', $id);
		$this->db->update($table, $data);
		$this->session->set_flashdata('alert', "success");
		$this->session->set_flashdata('msg', "Congratulations !! Updated successfully !!");
	}

	public function deleteRow($detail) {
		$table = $detail['table'];
		$id = $detail['id'];
		$this->db->where('id',$id);
		$this->db->delete($table);
		$this->session->set_flashdata('alert', "success");
		$this->session->set_flashdata('msg', "Congratulations !! Deleted successfully !!");
	}


	public function getAll($detail) {
		$table = $detail['table'];
		$this->db->select('*');
        $this->db->from($table);
        $query = $this->db->get();
        return $query->result();
	}


	public function getSingle($detail) {
		$table = $detail['table'];
		$item = $detail['item'];
		$value = $detail['value'];
		$query = $this->db->query("SELECT * FROM $table WHERE $item='$value'");
		return $query;
	}

}