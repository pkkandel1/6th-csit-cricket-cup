<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('homepage');
    }

   

  //   public function contact() {

         
  //       $data['page'] = $this->category_model->get_single_category_by_slug('contact');
		// //print_r($data);die();
  //       $data['category'] = $this->category_model->get_main_published_category();
		// $data['sub_category'] = $this->category_model->get_sub_published_category();
		// $data['feat_testimonial'] = $this->testimonial_model->get_featured_testimonials();
  //       $data['information'] = $this->info_model->get_single_info(1);
  //       $data['title'] = "Contact us - Cinemakers pvt.ltd";
  //       $this->load->view('contact', $data);
  //   }

  //   public function testimonials() {
  //       //$data['subpage'] = $this->page_model->get_subpage($data['page']->category_id);    
  //       $data['testimonial'] = $this->testimonial_model->get_published_testimonials();
        
  //       // $data['subpages'] = $this->page_model->get_related_subpages($slug);
        
  //       $data['category'] = $this->category_model->get_main_published_category();
		// $data['feat_testimonial'] = $this->testimonial_model->get_featured_testimonials();
		
		
		// //print_r($pid); die();
       
		
		// $data['sub_category'] = $this->category_model->get_sub_published_category();
		// $data['who'] = $this->page_model->get_single_published_page_category("who-we-are");
  //       $data['information'] = $this->info_model->get_single_info(1);
  //       $data['title'] = "What our clients say: client testimonials";
  //       $data['information'] = $this->info_model->get_single_info(1);
  //       $this->load->view('testimonial', $data);
  //   }

  //   public function testimonial_detail($slug) {
  //       $data['testimonial'] = $this->testimonial_model->get_single_published_testimonial($slug);
  //       if (empty($data['testimonial'])) {
  //           show_404();
  //       }
  //       $data['category'] = $this->category_model->get_main_published_category();
		// $data['sub_category'] = $this->category_model->get_sub_published_category();
		// $data['who'] = $this->page_model->get_single_published_page_category("who-we-are");
  //       $data['information'] = $this->info_model->get_single_info(1);
  //       $data['title'] = "success story of " . $data['testimonial']->person_name;
		// $data['feat_testimonial'] = $this->testimonial_model->get_featured_testimonials();
  //       $this->load->view('testimonial_detail', $data);
  //   }


    function send_contact() {
        $this->contact_model->send_contact(); // from ajax request
    }


}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */