<?php

class Category extends MX_Controller {

    function __construct() {
        parent::__construct();
	$this->load->library('image_moo');
        ob_start();
        $this->load->model('category/category_model');
        if (!$this->input->is_ajax_request()) {
            modules::run('user/auth/check_login');
        }
    }

    function index() {
        $this->getAll();
    }

   function do_upload() {
        $config['upload_path'] = "uploads/category_images/original";
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = '4000';
        $config['max_width'] = '2000';
        $config['max_height'] = '2000';
        $this->load->library('upload', $config);
        if ($this->upload->do_upload("userfile")) {
            $data = $this->upload->data();
            /* PATH */
            $source = "uploads/category_images/original/" . $data['file_name'];
            $destination_medium = "uploads/category_images/resized/";
            $destination_thumbs = "uploads/category_images/thumb/";
            $size_medium_width = 250;
            $size_medium_height = 125;
            $size_thumb_width = 70;
            $size_thumb_height = 70;
            $this->image_moo
                    ->load($source)
                    /* RESIZING IMAGE TO BE MEDIUM SIZE */
                    ->resize_crop($size_medium_width, $size_medium_height)
                    ->save($destination_medium . $data['file_name'])
                    /* CROPPING IMAGE TO BE THUMBNAIL SIZE */
                    ->resize_crop($size_thumb_width, $size_thumb_height)
                    ->save($destination_thumbs . $data['file_name']);
            if ($this->image_moo->errors)
                print $this->image_moo->display_errors();
            else {
                return $data['file_name'];
            }
        }
        else{
           $error = strip_tags($this->upload->display_errors());
          echo "<script type='text/javascript'>alert('.$error.');history.back(-1);</script>";
         die();          
        }
    }

    function add() {
        if ($_POST) {
	    $img = $this->do_upload();
            $this->category_model->manage($img);
            $this->getAll();
        } else {
            $data['title'] = "Add Category";
	    $data['parent'] = $this->category_model->getParent();
            $this->load->view('base/header', $data);
            $this->load->view('category_form');
            $this->load->view('base/footer');
        }
    }

    /*    Function to validate the post of the form     */

    function getAll() {
        $data['title'] = "View existing Pages";        
        $data['all'] = $this->category_model->getAll();
        $this->load->view('base/header', $data);
        $this->load->view('category/category_table');
        $this->load->view('base/table_footer');
    }

    /*
     * Function to update the destination 
     * id:destinaion identity  as argument 
     */

    function edit($id = '') {
//        $id = $this->uri->segment(3);
        if ($_POST) {
            $data = $this->category_model->getSingle($id);
	    $old = $data->image;
        if (!empty($_FILES['userfile']['name'])) {
            $img = $this->do_upload();
        } else {
            $img = $old;
        }
//print_r($img);die();
            $this->category_model->manage($img,$id);
            redirect('category');
        } else {
            $data['single'] = $this->category_model->getSingle($id);
           
            if (empty($data['single'])) {
                show_404();
            }
            $data['title'] = "Edit Category";
	    $data['parent'] = $this->category_model->getParent();
            $this->load->view('base/header', $data);
            $this->load->view('category_form');
            $this->load->view('base/footer');
        }
    }

    /*
     * Function to Delete category 
     * id:category identity  as argument 
     */

    function delete($id) {
        $this->category_model->delete($id);
        redirect('index.php/category');
    }

}

?>
